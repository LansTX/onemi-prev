<?php
/* Esta clase le dice a Yii si un Usuario es válido para iniciar sesión según los parametros que se establezcan */
class UserIdentity extends CUserIdentity
{
	public $_id; //Este valor es para identificar al Usuario dentro de Yii, idealmente el id del Usuario en la tabla
	// Existen 2 valores provenientes del formulario que representan al nombre de usuario y contraseña
	// $this->username y $this->password
	
	// Esta función se llama cuando se utiliza el formulario de login, para verificar que los datos son válidos
	public function authenticate()
	{
		// Se utiliza el modelo para buscar el usuario
		$Usuario = Usuario::model()->findByAttributes(array('loginUsuario'=>$this->username));
		
		if($Usuario===null){ // Se verifica que el Usuario exista en la bd
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}
		else if($Usuario->passUsuario !== $this->password){ // Se verifica que la contraseña coincida con el Usuario
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		}
		else{ // En el caso que no suceda lo anterior, el usuario puede loguearse 
			$this->errorCode=self::ERROR_NONE;			

			// _id se utiliza para representar al usuario y nombre para usarlo en Cerrar Sesion(nombre)
			$this->_id = $Usuario->idUsuario;
			$this->username = $Usuario->nombreUsuario;			
		}		

		return !$this->errorCode;
	}
	// DEBE EXISTIR esta función, Yii lo utiliza para obtener el id del Usuario
	public function getId()
	{
		return $this->_id;
	}
}