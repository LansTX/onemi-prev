<?php
// Esta clase permite agregar funciones de usuario 
// Ej: Comprobar si el usuario es Administrador

class WebUser extends CWebUser{	
	// Almacenar el modelo para no repetir la consulta.
	private $_modelo;

	// Carga el modelo del usuario.
	protected function cargarUsuario($id=null){
		if($this->_modelo===null)
		{
			if($id!==null) $this->_modelo=Usuario::model()->findByPk($id);
		}
		return $this->_modelo;
	}

	// Retorna el nombre del usuario, false si no encuentra el usuario
	public function getNombre(){
		$Nombre = false;
		if($this->_modelo === null) $this->_modelo = $this->cargarUsuario(Yii::app()->user->id);
		
		$Nombre = $this->_modelo->nombreUsuario;
		return $Nombre;
	}

	// Retorna el tipo de usuario, si no encuentra al Usuario retorna false
	public function tipoPerfil(){
		$tipoPerfil = false; 
		$Perfil = $this->cargarUsuario(Yii::app()->user->id);

		if($Perfil!=null) $tipoPerfil = $Perfil->tipoUsuario;

		return $tipoPerfil.'';
	}

	// Retorna verdadero si el usuario es Administrador (0)
	public function esAdmin(){
		if(!Yii::app()->user->isGuest){
			$tipoPerfil = $this->tipoPerfil();
						
			if($tipoPerfil == 0) return true;
			else return false;
		}
		else return false;
	}
}
?>