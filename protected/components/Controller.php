<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public $Logging = false; // Si es false para que no registre en el log.
	
	// Permite definir si se filtran las acciones para registrar en el log, si es true sólo se registran las acciones del arreglo $accionesRegistradas
	public $filtrarAcciones = true;
	// Permite definir que acciones se registran (minusculas)
	public $accionesRegistradas = array('nuevo', 'editar'); 

	// Permite definir si se filtran los controladores para registrar en el log, si es true sólo se registran los controladores del arreglo $controladoresRegistrados
	public $filtrarControladores = false;
	// Permite definir que controladores se registran (minusculas)
	public $controladoresRegistrados = array('productor');

	// Permite registrar cada acción del usuario, dependiendo de los filtros establecidos
	protected function afterAction($action){   
        if($this->Logging){
        	if(!Yii::app()->user->isGuest){
		        $accion = $action->id;
		        $controlador = $action->controller->id;
	        	$seRegistra = true;

	        	if($this->filtrarControladores){ if(!in_array(strtolower($controlador), $this->controladoresRegistrados)) $seRegistra = false;}
	        	if($this->filtrarAcciones){ if(!in_array(strtolower($accion), $this->accionesRegistradas)) $seRegistra = false;}

	        	if($seRegistra){
	        		$nuevoLog = new Registro;        
			        $nuevoLog->tiempoLog = date("Y-m-d H:i:s");
			        $nuevoLog->Usuarios_idUsuario = Yii::app()->user->id;
			        $nombreUsuario = Yii::app()->user->name;
			        $ip = $_SERVER['REMOTE_ADDR'];
			        if(strlen($ip)<=3) $ip = "local";
			        $EventoRegistrado = 'El usuario '.$nombreUsuario.' en la IP '.$ip.' utilizó la accion '.$accion.' del controlador '.$controlador.'. ';
			        $nuevoLog->registroLog = $EventoRegistrado;
			        $nuevoLog->save();
	        	}
	        }
        }
    }
}