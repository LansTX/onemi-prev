<?php
class Util{

    public static function sizeFile($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
	}


	public static function convertirFechaTexto($strFecha){
		$Texto = "";
		if(!empty($strFecha)){
			if(strlen($strFecha)>0 && strlen($strFecha)<20){
				$Fecha = strtotime($strFecha);
				if($Fecha === false || $Fecha < 0) return $Texto;

				$A = date('Y', $Fecha);
				$M = date('m', $Fecha);
				$D = date('d', $Fecha);
				$Mes = "";

				if($M == 1) $Mes = "Enero";
				else if ($M == 2) $Mes = "Febrero";
				else if ($M == 3) $Mes = "Marzo";
				else if ($M == 4) $Mes = "Abril";
				else if ($M == 5) $Mes = "Mayo";
				else if ($M == 6) $Mes = "Junio";
				else if ($M == 7) $Mes = "Julio";
				else if ($M == 8) $Mes = "Agosto";
				else if ($M == 9) $Mes = "Septiembre";
				else if ($M == 10) $Mes = "Octubre";
				else if ($M == 11) $Mes = "Noviembre";
				else $Mes = "Diciembre";

				$Texto = $D . " de " . $Mes . " del " . $A;
				return $Texto;
			}
		}

		return $Texto;
	}
	
	public static function tipoUsuario($tipoUsuario){

		$tipo = "";

		if($tipoUsuario == 0) $tipo = "Administrador";

		else if($tipoUsuario == 1) $tipo = "Coordinador";

		return $tipo;

	}

	//
	public static function CrearRespaldo($variable){
		$Valores = "";
		foreach($variable as $atributo => $valor){
			$Valores=$Valores."".$atributo.":".$valor." ";
		}
		return $Valores;		
	}	
	
	public static function ASDF($variable){
		var_dump($variable);
	}
	
	public static function StringAleatorio($length = 10) {
		$caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$aleatorio = '';
		for ($i = 0; $i < $length; $i++) {
			$aleatorio .= $caracteres[rand(0, strlen($caracteres) - 1)];
		}
		return $aleatorio;
	}
	
	//Retorna un arreglo con la diferencia en a�o, meses y dias
	public static function DiferenciaFechas($fecha1, $fecha2){
		$fecha1 = strtotime($fecha1);
		$fecha2 = strtotime($fecha2);
		if ($fecha1 === false || $fecha1 < 0 || $fecha2 === false || $fecha2 < 0 || $fecha1 > $fecha2)
			return false;
		 
		$years = date('Y', $fecha2) - date('Y', $fecha1);
		 
		$endMonth = date('m', $fecha2);
		$startMonth = date('m', $fecha1);
		 
		// Calculate months
		$months = $endMonth - $startMonth;
		if ($months <= 0)  {
			$months += 12;
			$years--;
		}
		if ($years < 0)
			return false;
		 
		// Calculate the days
		$offsets = array();
		if ($years > 0)
			$offsets[] = $years . (($years == 1) ? ' a�o' : ' a�os');
		if ($months > 0)
			$offsets[] = $months . (($months == 1) ? ' mes' : ' meses');
		$offsets = count($offsets) > 0 ? '+' . implode(' ', $offsets) : 'ahora';
		
		$days = $fecha2 - strtotime($offsets, $fecha1);
		$days = date('z', $days);
		 
		return array($years, $months, $days);
			
	}

	
}