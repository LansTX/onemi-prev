<?php

/**
 * This is the model class for table "capacitacion".
 *
 * The followings are the available columns in table 'capacitacion':
 * @property integer $idCapacitacion
 * @property string $descripcionCap
 * @property string $tituloCap
 * @property string $imagenCap
 * @property integer $Usuario_idUsuario
 * @property string $fechaCap
 * @property integer $estadoCap
 * @property string $fechaLimCap
 *
 * The followings are the available model relations:
 * @property Usuario $usuarioIdUsuario
 * @property Formcapacitacion[] $formcapacitacions
 */
class Capacitacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'capacitacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcionCap, tituloCap, imagenCap, Usuario_idUsuario, fechaCap, estadoCap, fechaLimCap', 'required'),
			array('Usuario_idUsuario, estadoCap', 'numerical', 'integerOnly'=>true),
			array('imagenCap', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'),
			array('imagenCap', 'length', 'max'=>255, 'on'=>'insert,update'),
			array('descripcionCap', 'length', 'max'=>15000),
			array('tituloCap', 'length', 'max'=>45),
			array('imagenCap', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idCapacitacion, descripcionCap, tituloCap, imagenCap, Usuario_idUsuario, fechaCap, estadoCap, fechaLimCap', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioIdUsuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_idUsuario'),
			'formcapacitacions' => array(self::HAS_MANY, 'Formcapacitacion', 'Capacitacion_idCapacitacion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCapacitacion' => 'Id Capacitacion',
			'descripcionCap' => 'Contenido',
			'tituloCap' => 'Título',
			'imagenCap' => 'Imagen',
			'Usuario_idUsuario' => 'Creado por',
			'fechaCap' => 'Fecha',
			'estadoCap' => 'Estado',
			'fechaLimCap' => 'Fecha Término',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCapacitacion',$this->idCapacitacion);
		$criteria->compare('descripcionCap',$this->descripcionCap,true);
		$criteria->compare('tituloCap',$this->tituloCap,true);
		$criteria->compare('imagenCap',$this->imagenCap,true);
		$criteria->compare('Usuario_idUsuario',$this->Usuario_idUsuario);
		$criteria->compare('fechaCap',$this->fechaCap,true);
		$criteria->compare('estadoCap',$this->estadoCap);
		$criteria->compare('fechaLimCap',$this->fechaLimCap,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Capacitacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	
}
