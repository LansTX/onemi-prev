<?php

/**
 * This is the model class for table "zonasegura".
 *
 * The followings are the available columns in table 'zonasegura':
 * @property integer $idZonaSegura
 * @property string $latitudZS
 * @property string $longitudZS
 * @property string $nombreZS
 * @property string $iconoZS
 * @property integer $Categoria_idCategoria
 * @property integer $Usuario_idUsuario
 *
 * The followings are the available model relations:
 * @property Categoria $categoriaIdCategoria
 * @property Usuario $usuarioIdUsuario
 */
class ZonaSegura extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'zonasegura';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('latitudZS, longitudZS, nombreZS, iconoZS, Categoria_idCategoria, Usuario_idUsuario', 'required'),
			array('Categoria_idCategoria, Usuario_idUsuario', 'numerical', 'integerOnly'=>true),
			array('nombreZS', 'length', 'max'=>45),
			array('latitudZS, longitudZS', 'length', 'max'=>13),
			array('iconoZS', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idZonaSegura, latitudZS, longitudZS, nombreZS, iconoZS, Categoria_idCategoria, Usuario_idUsuario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categoriaIdCategoria' => array(self::BELONGS_TO, 'Categoria', 'Categoria_idCategoria'),
			'usuarioIdUsuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_idUsuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idZonaSegura' => 'Idzonasegura',
			'latitudZS' => 'Latitud',
			'longitudZS' => 'Longitud',
			'nombreZS' => 'Nombre',
			'iconoZS' => 'Ícono',
			'Categoria_idCategoria' => 'Categoría',
			'Usuario_idUsuario' => 'Usuario Id Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idZonaSegura',$this->idZonaSegura);
		$criteria->compare('latitudZS',$this->latitudZS,true);
		$criteria->compare('longitudZS',$this->longitudZS,true);
		$criteria->compare('nombreZS',$this->nombreZS,true);
		$criteria->compare('iconoZS',$this->iconoZS,true);
		$criteria->compare('Categoria_idCategoria',$this->Categoria_idCategoria);
		$criteria->compare('Usuario_idUsuario',$this->Usuario_idUsuario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ZonaSegura the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getNombreCategoria($idCategoria){
		$nombre = null;
		$Cat = Categoria::model()->findByAttributes(array('idCategoria'=>$idCategoria));
		if($Cat != null){
			$nombre = $Cat->nombreCategoria;
		}
		return $nombre;
	}
}
