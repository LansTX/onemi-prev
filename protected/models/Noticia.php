<?php

/**
 * This is the model class for table "noticia".
 *
 * The followings are the available columns in table 'noticia':
 * @property integer $idNoticia
 * @property string $tituloNoticia
 * @property string $subtituloNoticia
 * @property string $fechaNoticia
 * @property string $cuerpoNoticia
 * @property string $imagenNoticia
 * @property integer $Usuario_idUsuario
 *
 * The followings are the available model relations:
 * @property Usuario $usuarioIdUsuario
 */
class Noticia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'noticia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tituloNoticia, fechaNoticia, cuerpoNoticia, imagenNoticia, Usuario_idUsuario', 'required'),
			array('Usuario_idUsuario', 'numerical', 'integerOnly'=>true),
			array('imagenNoticia', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'),
			array('imagenNoticia', 'length', 'max'=>255, 'on'=>'insert,update'),
			array('tituloNoticia', 'length', 'max'=>300),
			array('subtituloNoticia', 'length', 'max'=>500),
			array('cuerpoNoticia', 'length', 'max'=>15000),
			array('imagenNoticia', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idNoticia, tituloNoticia, subtituloNoticia, fechaNoticia, cuerpoNoticia, imagenNoticia, Usuario_idUsuario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioIdUsuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_idUsuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idNoticia' => 'Id Noticia',
			'tituloNoticia' => 'Título de la Noticia',
			'subtituloNoticia' => 'Subtítulo Noticia',
			'fechaNoticia' => 'Fecha de Creación',
			'cuerpoNoticia' => 'Contenido',
			'imagenNoticia' => 'Imagen',
			'Usuario_idUsuario' => 'Creado por',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idNoticia',$this->idNoticia);
		$criteria->compare('tituloNoticia',$this->tituloNoticia,true);
		$criteria->compare('subtituloNoticia',$this->subtituloNoticia,true);
		$criteria->compare('fechaNoticia',$this->fechaNoticia,true);
		$criteria->compare('cuerpoNoticia',$this->cuerpoNoticia,true);
		$criteria->compare('imagenNoticia',$this->imagenNoticia,true);
		$criteria->compare('Usuario_idUsuario',$this->Usuario_idUsuario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Noticia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
