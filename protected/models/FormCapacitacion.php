<?php

/**
 * This is the model class for table "formcapacitacion".
 *
 * The followings are the available columns in table 'formcapacitacion':
 * @property integer $idFormCapacitacion
 * @property string $nombreForm
 * @property string $apellidoForm
 * @property string $emailForm
 * @property integer $celularForm
 * @property string $razonForm
 * @property string $cargoForm
 * @property integer $Capacitacion_idCapacitacion
 *
 * The followings are the available model relations:
 * @property Capacitacion $capacitacionIdCapacitacion
 */
class FormCapacitacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'formcapacitacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombreForm, apellidoForm, emailForm, celularForm, razonForm, cargoForm, Capacitacion_idCapacitacion', 'required'),
			array(' Capacitacion_idCapacitacion', 'numerical', 'integerOnly'=>true),
			array('nombreForm, apellidoForm, celularForm, emailForm, razonForm, cargoForm', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idFormCapacitacion, nombreForm, apellidoForm, emailForm, celularForm, razonForm, cargoForm, Capacitacion_idCapacitacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'capacitacionIdCapacitacion' => array(self::BELONGS_TO, 'Capacitacion', 'Capacitacion_idCapacitacion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idFormCapacitacion' => 'Id Form Capacitacion',
			'nombreForm' => 'Nombre',
			'apellidoForm' => 'Apellido',
			'emailForm' => 'Email',
			'celularForm' => 'Celular',
			'razonForm' => 'Razon',
			'cargoForm' => 'Cargo',
			'Capacitacion_idCapacitacion' => 'Capacitación',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idFormCapacitacion',$this->idFormCapacitacion);
		$criteria->compare('nombreForm',$this->nombreForm,true);
		$criteria->compare('apellidoForm',$this->apellidoForm,true);
		$criteria->compare('emailForm',$this->emailForm,true);
		$criteria->compare('celularForm',$this->celularForm);
		$criteria->compare('razonForm',$this->razonForm,true);
		$criteria->compare('cargoForm',$this->cargoForm,true);
		$criteria->compare('Capacitacion_idCapacitacion',$this->Capacitacion_idCapacitacion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FormCapacitacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
