<?php

/**
 * This is the model class for table "alerta".
 *
 * The followings are the available columns in table 'alerta':
 * @property integer $idAlerta
 * @property string $mensajeAlerta
 * @property string $imagenAlerta
 * @property string $estadoAlerta
 * @property string $fechaAlerta
 * @property string $ordenAlerta
 * @property integer $Usuario_idUsuario
 *
 * The followings are the available model relations:
 * @property Usuario $usuarioIdUsuario
 * @property Tipoalerta[] $tipoalertas
 */
class Alerta extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'alerta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mensajeAlerta, imagenAlerta, estadoAlerta, fechaAlerta, Usuario_idUsuario, ordenAlerta', 'required'),
			array('idAlerta, Usuario_idUsuario', 'numerical', 'integerOnly'=>true),
			array('imagenAlerta', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'),
			array('imagenAlerta', 'length', 'max'=>255, 'on'=>'insert,update'),
			array('imagenAlerta, estadoAlerta, fechaAlerta, ordenAlerta', 'length', 'max'=>45),
			array('mensajeAlerta', 'length', 'max'=>5000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idAlerta, mensajeAlerta, imagenAlerta, estadoAlerta, fechaAlerta, ordenAlerta, Usuario_idUsuario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioIdUsuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_idUsuario'),
			'tipoalertas' => array(self::HAS_MANY, 'Tipoalerta', 'Alerta_idAlerta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idAlerta' => 'Idalerta',
			'mensajeAlerta' => 'Mensaje de Alerta',
			'imagen Alerta' => 'Icono',
			'estadoAlerta' => 'Estado',
			'fechaAlerta' => 'Fecha',
			'ordenAlerta' => 'Tipo de Alerta',
			'Usuario_idUsuario' => 'Creado por',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idAlerta',$this->idAlerta);
		$criteria->compare('mensajeAlerta',$this->mensajeAlerta,true);
		$criteria->compare('imagenAlerta',$this->imagenAlerta,true);
		$criteria->compare('estadoAlerta',$this->estadoAlerta,true);
		$criteria->compare('fechaAlerta',$this->fechaAlerta,true);
		$criteria->compare('Usuario_idUsuario',$this->Usuario_idUsuario);
		$criteria->compare('ordenAlerta',$this->ordenAlerta,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Alerta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
