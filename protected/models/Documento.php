<?php

/**
 * This is the model class for table "documento".
 *
 * The followings are the available columns in table 'documento':
 * @property integer $idDocumento
 * @property string $rutaDoc
 * @property string $nombreDoc
 * @property string $sizeDoc
 * @property string $fechaDoc
 * @property integer $CategoriaDocumento_idCategoriaDocumento
 * @property integer $Usuario_idUsuario
 *
 * The followings are the available model relations:
 * @property Categoriadocumento $categoriaDocumentoIdCategoriaDocumento
 * @property Usuario $usuarioIdUsuario
 */
class Documento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'documento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rutaDoc, nombreDoc, sizeDoc, fechaDoc, CategoriaDocumento_idCategoriaDocumento, Usuario_idUsuario', 'required'),
			array('CategoriaDocumento_idCategoriaDocumento, Usuario_idUsuario', 'numerical', 'integerOnly'=>true),
			array('rutaDoc', 'file','types'=>'pdf', 'allowEmpty'=>true, 'on'=>'update'),
			array('rutaDoc', 'length', 'max'=>255, 'on'=>'insert,update'),
			array('rutaDoc, nombreDoc, sizeDoc', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idDocumento, rutaDoc, nombreDoc, sizeDoc, fechaDoc, CategoriaDocumento_idCategoriaDocumento, Usuario_idUsuario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categoriaDocumentoIdCategoriaDocumento' => array(self::BELONGS_TO, 'Categoriadocumento', 'CategoriaDocumento_idCategoriaDocumento'),
			'usuarioIdUsuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_idUsuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idDocumento' => 'Id Documento',
			'rutaDoc' => 'Documento',
			'nombreDoc' => 'Nombre',
			'sizeDoc' => 'Tamaño',
			'fechaDoc' => 'Fecha',
			'CategoriaDocumento_idCategoriaDocumento' => 'Categoría',
			'Usuario_idUsuario' => 'Subido por',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idDocumento',$this->idDocumento);
		$criteria->compare('rutaDoc',$this->rutaDoc,true);
		$criteria->compare('nombreDoc',$this->nombreDoc,true);
		$criteria->compare('sizeDoc',$this->sizeDoc,true);
		$criteria->compare('fechaDoc',$this->fechaDoc,true);
		$criteria->compare('CategoriaDocumento_idCategoriaDocumento',$this->CategoriaDocumento_idCategoriaDocumento);
		$criteria->compare('Usuario_idUsuario',$this->Usuario_idUsuario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Documento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getNombreCategoria($idCategoria){
		$nombre = null;
		$Cat = CategoriaDocumento::model()->findByAttributes(array('idCategoriaDocumento'=>$idCategoria));
		if($Cat != null){
			$nombre = $Cat->nombreCatDoc;
		}
		return $nombre;
	}

	
}
