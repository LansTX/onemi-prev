<?php

/* 
Esta clase es un modelo que guarda la estructura del formulario de login, usado por SiteController.
*/
class LoginForm extends CFormModel
{
	// Si se cambia el nombre de estos valores, tambi�n modificar en SiteController
	public $usuario; 
	public $pass;  
	//public $recordar; //Este valor se puede usar para que el sitio loguee de forma automatica al Usuario

	private $_identity;

	// Se declaran las reglas de validaci�n
	public function rules()
	{
		return array(
			array('usuario, pass', 'required'), // nombre de usuario y contrase�a son necesarios
			array('usuario', 'length', 'max'=>50),
			array('pass', 'length', 'max'=>100),
			array('pass', 'authenticate'), // Se verifica la contrase�a
		);
	}

	public function attributeLabels()
	{
		// Etiquetas para reemplazar en el formulario de login
		return array(
			'usuario'=>'Usuario',
			'pass' => utf8_encode('Contrase�a'),
		);
	}

	// Se envia los datos del formulario a UserIdentity 
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->usuario,$this->pass);
			if(!$this->_identity->authenticate())
				$this->addError('pass',utf8_encode('Nombre de usuario o contrase�a incorrecto.'));
		}
	}
 
 	// Retorna true si el Usuario se loguea sin problemas
	public function login()
	{
		if($this->_identity===null)
		{
			// Se envian los datos ingresados en el formulario a UserIdentity para realizar la comprobaci�n
			$this->_identity=new UserIdentity($this->usuario,$this->pass);
			$this->_identity->authenticate();			
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duracion=3600*24*30; // Loguea por 30 d�as al usuario
			Yii::app()->user->login($this->_identity,$duracion);
			return true;
		}
		else
			return false;
	}

	public function VerificarRut($attribute, $params){
		$RutIngresado = $this->usuario;
		$Numero = substr($RutIngresado, 0,-2);
		$DV = substr($RutIngresado, -1);
		$Error = true;
		
		if(!isset($Numero) || strlen($Numero)==0 || strlen($Numero)>8) 
			$Error = true;
		else{
			if($this->DigitoVerificador($Numero)==strtolower($DV)) $Error = false;
		}		
		
		if($Error) $this->addError($attribute, utf8_encode("El Rut ingresado no es v�lido"));
	}
	
	public function DigitoVerificador($numero){
		$factor = 2; $suma=0;
		for($i = strlen($numero)-1; $i >= 0; $i--):
			$factor = $factor > 7 ? 2 : $factor;
			$suma += $numero{$i}*$factor++;
		endfor;
		
		$resto = $suma % 11;
		$dv = 11 - $resto;
		
		if($dv == 11){
			$dv=0;
		}else if($dv == 10){
			$dv="k";
		}
		
		return $dv;
	}
}
