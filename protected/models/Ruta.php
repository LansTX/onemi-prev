<?php

/**
 * This is the model class for table "ruta".
 *
 * The followings are the available columns in table 'ruta':
 * @property integer $idRuta
 * @property string $nombreRuta
 * @property string $fechaRuta
 * @property string $textoRuta
 * @property integer $usuario_idUsuario
 *
 * The followings are the available model relations:
 * @property Puntoruta[] $puntorutas
 * @property Usuario $usuarioIdUsuario
 */
class Ruta extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ruta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombreRuta, fechaRuta, usuario_idUsuario', 'required'),
			array('usuario_idUsuario', 'numerical', 'integerOnly'=>true),
			array('nombreRuta, fechaRuta', 'length', 'max'=>45),
			array('textoRuta', 'length', 'max'=>50000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idRuta, nombreRuta, fechaRuta, textoRuta, usuario_idUsuario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'puntorutas' => array(self::HAS_MANY, 'Puntoruta', 'ruta_idRuta'),
			'usuarioIdUsuario' => array(self::BELONGS_TO, 'Usuario', 'usuario_idUsuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idRuta' => 'Id Ruta',
			'nombreRuta' => 'Nombre de la Ruta',
			'fechaRuta' => 'Fecha',
			'textoRuta' => 'Texto',
			'usuario_idUsuario' => 'Usuario Id Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idRuta',$this->idRuta);
		$criteria->compare('nombreRuta',$this->nombreRuta,true);
		$criteria->compare('fechaRuta',$this->fechaRuta,true);
		$criteria->compare('textoRuta',$this->textoRuta,true);
		$criteria->compare('usuario_idUsuario',$this->usuario_idUsuario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ruta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
