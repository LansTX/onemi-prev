<?php

/**
 * This is the model class for table "usuario".
 *
 * The followings are the available columns in table 'usuario':
 * @property integer $idUsuario
 * @property string $nombreUsuario
 * @property string $loginUsuario
 * @property string $passUsuario
 * @property integer $tipoUsuario
 *
 * The followings are the available model relations:
 * @property Zonasegura[] $zonaseguras
 */
class Usuario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombreUsuario, loginUsuario, passUsuario, tipoUsuario', 'required'),
			array('tipoUsuario', 'numerical', 'integerOnly'=>true),
			array('nombreUsuario, loginUsuario', 'length', 'max'=>50),
			array('passUsuario', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idUsuario, nombreUsuario, loginUsuario, passUsuario, tipoUsuario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'zonaseguras' => array(self::HAS_MANY,'Zonasegura', 'usuario_idUsuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idUsuario' => 'Id Usuario',
			'nombreUsuario' => 'Nombre de Usuario',
			'loginUsuario' => 'Login del Usuario',
			'passUsuario' => 'Contraseña del Usuario',
			'tipoUsuario' => 'Tipo de Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idUsuario',$this->idUsuario);
		$criteria->compare('nombreUsuario',$this->nombreUsuario,true);
		$criteria->compare('loginUsuario',$this->loginUsuario,true);
		$criteria->compare('passUsuario',$this->passUsuario,true);
		$criteria->compare('tipoUsuario',$this->tipoUsuario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getNombreUsuario($idUsuario){
		$nombre = null;
		$Cat = Usuario::model()->findByAttributes(array('idUsuario'=>$idUsuario));
		if($Cat != null){
			$nombre = $Cat->nombreUsuario;
		}
		return $nombre;
	}
}
