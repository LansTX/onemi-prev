<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idTipoAlerta')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idTipoAlerta),array('view','id'=>$data->idTipoAlerta)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombreTipoAlerta')); ?>:</b>
	<?php echo CHtml::encode($data->nombreTipoAlerta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Alerta_idAlerta')); ?>:</b>
	<?php echo CHtml::encode($data->Alerta_idAlerta); ?>
	<br />


</div>