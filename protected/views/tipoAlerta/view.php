<?php
$this->breadcrumbs=array(
	'Tipo Alertas'=>array('index'),
	$model->idTipoAlerta,
);

$this->menu=array(
	array('label'=>'Listar TipoAlerta','url'=>array('index')),
	array('label'=>'Crear TipoAlerta','url'=>array('create')),
	array('label'=>'Modificar TipoAlerta','url'=>array('update','id'=>$model->idTipoAlerta)),
	array('label'=>'Eliminar TipoAlerta','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idTipoAlerta),'confirm'=>'Esta seguro de eliminar este item?')),
	array('label'=>'Administrar TipoAlerta','url'=>array('admin')),
);
?>

<h1>Viendo TipoAlerta #<?php echo $model->idTipoAlerta; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'idTipoAlerta',
		'nombreTipoAlerta',
		'Alerta_idAlerta',
	),
)); ?>
