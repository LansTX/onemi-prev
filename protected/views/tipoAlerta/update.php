<?php
$this->breadcrumbs=array(
	'Tipo Alertas'=>array('index'),
	$model->idTipoAlerta=>array('view','id'=>$model->idTipoAlerta),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar TipoAlerta','url'=>array('index')),
	array('label'=>'Crear TipoAlerta','url'=>array('create')),
	array('label'=>'Ver TipoAlerta','url'=>array('view','id'=>$model->idTipoAlerta)),
	array('label'=>'Administrar TipoAlerta','url'=>array('admin')),
);
?>

<h1>Modificar TipoAlerta <?php echo $model->idTipoAlerta; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>