<?php
$this->breadcrumbs=array(
	'Tipo Alertas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar TipoAlerta','url'=>array('index')),
	array('label'=>'Administrar TipoAlerta','url'=>array('admin')),
);
?>

<h1>Nuevo Tipo de Alerta</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>