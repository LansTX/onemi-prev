<script type="text/javascript">
function drawFreeHand()
{

    //the polygon
    poly=new google.maps.Polyline({strokeColor: "#55acee",strokeWeight: 5, map:map,clickable:false});
    
    //move-listener
    var move=google.maps.event.addListener(map,'mousemove',function(e){
        poly.getPath().push(e.latLng);
    });
    
    //mouseup-listener
    google.maps.event.addListenerOnce(map,'mouseup',function(e){
        google.maps.event.removeListener(move);
        var path=poly.getPath();
        poly.setMap(null);
        poly=new google.maps.Polygon({strokeColor: "#55acee",strokeWeight: 5, fillColor: "#55acee",map:map,path:path});
        
        
        google.maps.event.clearListeners(map.getDiv(), 'mousedown');
        
        enable()
    });
}

function disable(){
  map.setOptions({
    draggable: false, 
    zoomControl: false, 
    scrollwheel: false, 
    disableDoubleClickZoom: false
  });
}

function enable(){
  map.setOptions({
    draggable: true, 
    zoomControl: true, 
    scrollwheel: true, 
    disableDoubleClickZoom: true
  });
}


function initialize() 
{
  var mapOptions = {
    zoom: 14,
    center: new google.maps.LatLng(-18.487171, -70.306298),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
       
/*
  var drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.MARKER,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [
        //google.maps.drawing.OverlayType.MARKER,
        google.maps.drawing.OverlayType.CIRCLE,
        google.maps.drawing.OverlayType.POLYGON,
        //google.maps.drawing.OverlayType.POLYLINE,
        google.maps.drawing.OverlayType.RECTANGLE
      ]
    },
    rectangleOptions:{
      editable: true,
      fillColor: '#55acee',
    },
    markerOptions: {icon: 'images/beachflag.png'},
    circleOptions: {
      fillColor: '#55acee',
      strokeWeight: 5,
      clickable: false,
      editable: true,
      zIndex: 1
    }
  });
  drawingManager.setMap(map);

*/

  //draw
  $("#drawpoly a").click(function(e) {
    e.preventDefault();

    disable()

    google.maps.event.addDomListener(map.getDiv(),'mousedown',function(e){
      drawFreeHand()
    });

  });

  $("#reset").click(function(e) {
    initialize();

  });

}

//google.maps.event.addDomListener(window, 'load', initialize);

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9_FJ8r7K4pNJPfNLOrYsY1sk6fyEdtP0&libraries=geometry,places,drawing&callback=initialize" async defer></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9_FJ8r7K4pNJPfNLOrYsY1sk6fyEdtP0&libraries=geometry,places&ext=.js&callback=initialize"></script>-->

<div id="drawpoly" class="botonForm" style="font-size:20px; display:inline-block;"><a style="color:white" href="#">Lápiz</a></div>
<div id="reset" class="botonForm" style="margin-bottom: 20px;font-size:20px; display:inline-block;"><a style="color:white" href="#">Reiniciar</a></div>
<br/>
<div id="map_canvas" style="height:500px; width:80%;"></div>