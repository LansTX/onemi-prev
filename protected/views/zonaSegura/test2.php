<script type="text/javascript">

function initialize() 
{
  var mapOptions = {
    zoom: 14,
    center: new google.maps.LatLng(-18.487171, -70.306298),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
       

  var drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.MARKER,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [
        //google.maps.drawing.OverlayType.MARKER,
        google.maps.drawing.OverlayType.CIRCLE,
        google.maps.drawing.OverlayType.POLYGON,
        //google.maps.drawing.OverlayType.POLYLINE,
        google.maps.drawing.OverlayType.RECTANGLE
      ]
    },
    polygonOptions:{
      editable: true,
      fillColor: '#55acee',
      strokeColor: '#55acee',
      strokeWeight: 5,
    },
    rectangleOptions:{
      editable: true,
      fillColor: '#55acee',
      strokeColor: "#55acee",
      strokeWeight: 5,
    },
    circleOptions: {
      fillColor: '#55acee',
      strokeColor: "#55acee",
      strokeWeight: 5,
      clickable: false,
      editable: true,
      zIndex: 1
    }
  });
  drawingManager.setMap(map);


  $("#reset").click(function(e) {
    initialize();

  });

}

//google.maps.event.addDomListener(window, 'load', initialize);

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9_FJ8r7K4pNJPfNLOrYsY1sk6fyEdtP0&libraries=geometry,places,drawing&callback=initialize" async defer></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9_FJ8r7K4pNJPfNLOrYsY1sk6fyEdtP0&libraries=geometry,places&ext=.js&callback=initialize"></script>-->

<div id="reset" class="botonForm" style="font-size:20px; display:inline-block;"><a style="color:white;"href="#">Reiniciar</a></div>
<br/>
<br/><br/>
<div id="map_canvas" style="height:500px; width:80%;"></div>