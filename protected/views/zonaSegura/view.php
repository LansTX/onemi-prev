
<?php

$this->breadcrumbs=array(
	'Zona Seguras'=>array('index'),
	$model->idZonaSegura,
);

$this->menu=array(
	array('label'=>'Listar ZonaSegura','url'=>array('index')),
	array('label'=>'Crear ZonaSegura','url'=>array('create')),
	array('label'=>'Modificar ZonaSegura','url'=>array('update','id'=>$model->idZonaSegura)),
	array('label'=>'Eliminar ZonaSegura','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idZonaSegura),'confirm'=>'Esta seguro de eliminar este item?')),
	//array('label'=>'Administrar ZonaSegura','url'=>array('admin')),
);
?>



<h1><?php echo $model->nombreZS; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		//'idzonasegura',
		'latitudZS',
		'longitudZS',
		'nombreZS',
		//'iconoZS',
		'Categoria_idCategoria'=>array(
			'name'=>'Categoria_idCategoria',
			'value'=>ZonaSegura::getNombreCategoria($model->Categoria_idCategoria),
			),

		//'usuario_idUsuario',
	),
)); ?>





	<div style="width:90%; height:400px; margin: auto;" id="map">

	</div>

<script>
<?php 
echo "

	function initMap() {

        var mapDiv = document.getElementById('map');

        //añadir mapa
        var map = new google.maps.Map(mapDiv, {
          center: {lat: ".$model->latitudZS.", lng: ".$model->longitudZS."},
          zoom: 15
        });

        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';

        //ubicación de la marca de la bandera/escuela
        var myLatLng = {lat: ".$model->latitudZS.", lng: ".$model->longitudZS."};
        var marker = new google.maps.Marker({
  			position: myLatLng,
  			map: map,
  			icon: iconBase + 'schools_maps.png'
  		});

  		var latLng = marker.getPosition();
		map.setCenter(latLng);
    }

	$(document).ready(function(){
	});
        
	
       
";
?>
</script>

	


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9_FJ8r7K4pNJPfNLOrYsY1sk6fyEdtP0&callback=initMap" async defer></script>
