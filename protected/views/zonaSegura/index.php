<?php
$this->breadcrumbs=array(
	'Zona Seguras',
);

$this->menu=array(
	array('label'=>'Nueva ZonaSegura','url'=>array('create')),
	//array('label'=>'Administrar ZonaSegura','url'=>array('admin')),
);
?>

<h1>Zonas Seguras</h1>

<div class="coloradd icon-block">
<a href="<?php echo Yii::app()->createUrl('zonasegura/create',array());?>">
<img class="mas" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/mas.png">
<span>Nuevo</span>
</a>
</div>
<table class="responstable" style="margin-top: 60px;">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Latitud</th>
			<th>Longitud</th>
			<th>Categoría</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$lista = ZonaSegura::model()->findAll();
	if($lista)
	{
		foreach ($lista as $Item) {
			echo '<tr>';
			echo '<td>';
			echo $Item->nombreZS;
			echo '</td>';
			echo '<td>';
			echo $Item->latitudZS;
			echo '</td>';
			echo '<td>';
			echo $Item->longitudZS;
			echo '</td>';
			echo '<td>';
			echo CHtml::link(ZonaSegura::getNombreCategoria($Item->Categoria_idCategoria),Yii::app()->createUrl("categoria/view/".CHtml::encode($Item->Categoria_idCategoria)));
			echo '</td>';
			
			echo '<td class="opcionestable">';
			echo '<a href="'.Yii::app()->createUrl('zonasegura/view',array('id'=>$Item->idZonaSegura)).'"><img class="imagenView" src="/onemi-prev/themes/bootstrap/img/ver.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('zonasegura/update',array('id'=>$Item->idZonaSegura)).'"><img class="imagenView" src="/onemi-prev/themes/bootstrap/img/editar.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('zonasegura/delete',array('id'=>$Item->idZonaSegura)).'"><img class="imagenView" src="/onemi-prev/themes/bootstrap/img/borrar.png"></a>';
			echo '<i  aria-hidden="true"></i>';
			echo '</td>';
			echo '</tr>';
		}
	}
	?>
	
		
	</tbody>
</table>


<?php 

//$this->widget('bootstrap.widgets.TbListView',array(	'dataProvider'=>$dataProvider,	'itemView'=>'_view',)); 

?>
