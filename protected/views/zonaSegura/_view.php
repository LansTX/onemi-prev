<div class="view">

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$data,
	'attributes'=>array(
		array(
			'label'=>'Link',
			'name'=>'Link',
			'type'=>'raw',
			'value'=>CHtml::link("Ver Zona Segura",Yii::app()->createUrl("zonasegura/view/".CHtml::encode($data->idZonaSegura))),
			),
		'latitud',
		'longitud',
		'nombre',
		'icono',
		'categoria_idcategoria'=>array(
			'name'=>'Categoria_idCategoria',
			'value'=>ZonaSegura::getNombreCategoria($data->Categoria_idCategoria),
			),
		//'usuario_idUsuario',
	),
)); ?>

</div>