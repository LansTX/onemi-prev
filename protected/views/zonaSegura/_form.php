
<div style="width: 40%; display: inline-block;">
<?php 

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'zona-segura-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<?php echo $form->textFieldRow($model,'nombreZS',array('class'=>'span3','maxlength'=>45)); ?>

	<?php echo $form->hiddenField($model,'iconoZS',array('value'=>'testing','maxlength'=>200)); ?>

	<!--<?php echo $form->textFieldRow($model,'Categoria_idCategoria',array('class'=>'span3')); ?>-->

	<?php echo $form->dropdownlistrow($model,'Categoria_idCategoria',CHtml::listData( Categoria::model()->findAll(), 'idCategoria', 'nombreCategoria'),array('class'=>'span3',"style"=>"margin-left: 5px;")); ?>

	<?php echo $form->hiddenField($model,'Usuario_idUsuario',array('value'=>Yii::app()->user->getId())); ?>
	<?php echo $form->hiddenField($model,'latitudZS',array('maxlength'=>13)); ?>
	<?php echo $form->hiddenField($model,'longitudZS',array('maxlength'=>13)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			'htmlOptions'=>array(
				'class'=>'botonForm',
				),
		)); ?>
	</div>

<?php $this->endWidget(); ?>


</div>

<div style="width: 50%; display: inline-block; margin-left: 50px;float:right;">

	<div id="mapdiv" style="width:100%; height:400px;">
        
    </div>

	<script type="text/javascript">
	  	var map;
	  	var created = false;
	  	var lastmarker;
	  	function mapa()
	  	{
	    //var opts = {'center': new google.maps.LatLng(26.12295, -80.17122), 'zoom':11, 'mapTypeId': google.maps.MapTypeId.ROADMAP }
	   	var opts = {'center': new google.maps.LatLng(-18.487171, -70.306298), 'zoom':13, 'mapTypeId': google.maps.MapTypeId.ROADMAP }
	   	map = new google.maps.Map(document.getElementById('mapdiv'),opts);
	   	var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
	    google.maps.event.addListener(map,'click',function(event) {
	    	if (created) {
 				lastmarker.setMap(null);
 				
			}
	    	
	    	var str = event.latLng.lat();
	    	var long = event.latLng.lng();
	    	document.getElementById('ZonaSegura_longitudZS').value = long.toString().substring(0, 11); 
	    	document.getElementById('ZonaSegura_latitudZS').value = str.toString().substring(0, 11); 

	    	var posicion =  new google.maps.LatLng( str, long );

	    	lastmarker = new google.maps.Marker({
	    	
  			position: posicion,
  			map: map,
  			icon: iconBase + 'info-i_maps.png'
  				});
  			created=true;
	   	});
	   	
	   	<?php 

	   	if ($model->latitudZS != '' && $model->longitudZS != ''){
	   		echo "
	   		var myLatLng = {lat: ".$model->latitudZS.", lng: ".$model->longitudZS."};
        		var marker = new google.maps.Marker({
  			position: myLatLng,
  			map: map,
  			icon: iconBase + 'schools_maps.png'
  				});
  			var latLng = marker.getPosition();
			map.setCenter(latLng);
			  map.setZoom(15);
  				";
	   	}
	   	?>
	  	}
  	</script>
  	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9_FJ8r7K4pNJPfNLOrYsY1sk6fyEdtP0&callback=mapa" async defer></script>
</div>