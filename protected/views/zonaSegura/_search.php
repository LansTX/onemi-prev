<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'idZonaSegura',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'latitudZS',array('class'=>'span5','maxlength'=>11)); ?>

	<?php echo $form->textFieldRow($model,'longitudZS',array('class'=>'span5','maxlength'=>11)); ?>

	<?php echo $form->textFieldRow($model,'nombreZS',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'iconoZS',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'Categoria_idCategoria',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'Usuario_idUsuario',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
