<?php
/*
$this->breadcrumbs=array(
	'Zona Seguras'=>array('index'),
	$model->idzonasegura=>array('view','id'=>$model->idzonasegura),
	'Update',
);
*/
$this->menu=array(
	array('label'=>'Listar ZonaSegura','url'=>array('index')),
	array('label'=>'Nueva ZonaSegura','url'=>array('create')),
	array('label'=>'Ver ZonaSegura','url'=>array('view','id'=>$model->idZonaSegura)),
	//array('label'=>'Administrar ZonaSegura','url'=>array('admin')),
);
?>

<h1>Editando <?php echo $model->nombreZS; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>