<?php
$this->breadcrumbs=array(
	'Usuarios',
);

$this->menu=array(
	array('label'=>'Nuevo Usuario','url'=>array('create')),
	//array('label'=>'Administrar Usuario','url'=>array('admin')),
);
?>

<h1>Lista de Usuarios</h1>
<div class="coloradd icon-block">
<a href="<?php echo Yii::app()->createUrl('usuario/create',array());?>">
<img class="mas" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/mas.png">
<span>Nuevo</span>
</a>
</div>

<table class="responstable" style="margin-top: 60px;">
	<thead>
		<tr>
			<th>Nombre de Usuario</th>
			<th>ID</th>
			<th>Tipo de Usuario</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$listaUsuarios = Usuario::model()->findAll();
	if($listaUsuarios)
	{
		foreach ($listaUsuarios as $Usuario) {
			echo '<tr>';
			echo '<td>';
			echo $Usuario->nombreUsuario;
			echo '</td>';
			echo '<td>';
			echo $Usuario->loginUsuario;
			echo '</td>';
			echo '<td>';
			echo Util::tipoUsuario($Usuario->tipoUsuario);
			echo '</td>';
			
			echo '<td class="opcionestable">';
			/*
			echo '<a href="'.Yii::app()->createUrl('usuario/view',array('id'=>$Usuario->idUsuario)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/ver.png"></a>';
			*/
			echo '<a href="'.Yii::app()->createUrl('usuario/update',array('id'=>$Usuario->idUsuario)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/editar.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('usuario/delete',array('id'=>$Usuario->idUsuario)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/borrar.png"></a>';
			echo '<i  aria-hidden="true"></i>';
			echo '</td>';
			echo '</tr>';
		}
	}
	?>
	
		
	</tbody>
</table>

<?php 
//$this->widget('bootstrap.widgets.TbListView',array(	'dataProvider'=>$dataProvider,	'itemView'=>'_view',)); 
?>
