<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'usuario-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nombreUsuario',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'loginUsuario',array('class'=>'span5','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'passUsuario',array('class'=>'span5','maxlength'=>100)); ?>
	<?php echo $form->passwordFieldRow($model,'passUsuario',array('class'=>'span5','maxlength'=>100)); ?>

	<?php //echo $form->textFieldRow($model,'tipoUsuario',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'tipoUsuario',array('class'=>'span5')); 

		$seleccionado = array();

		if(!$model->isNewRecord){

	$seleccionado = array($model->tipoUsuario=>array('selected'=>true));

		}

		
		$Pregunta = "Escoja el tipo de Usuario";

		$ListaOpciones = array('0'=>"Administrador", '1'=>"Coordinador");
		echo $form->dropDownListRow($model,'tipoUsuario',$ListaOpciones,array('options'=>$seleccionado,'class'=>'span5', 'prompt'=>$Pregunta));

	

	?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			'htmlOptions'=>array(
				'class'=>'botonForm',
				),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
