<?php

$this->menu=array(
	array('label'=>'Listado Usuario','url'=>array('index')),
	array('label'=>'Nuevo Usuario','url'=>array('create')),
	array('label'=>'Editar Usuario','url'=>array('update','id'=>$model->idUsuario)),
	array('label'=>'Eliminar Usuario','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idUsuario),'confirm'=>'Esta seguro de eliminar este item?')),
	array('label'=>'Administrar Usuario','url'=>array('admin')),
);
?>

<h1><?php echo $model->nombreUsuario; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		//'idUsuario',
		'nombreUsuario',
		'loginUsuario',
		//'passUsuario',
		'tipoUsuario'=>array(
            'type'=>'raw',
            'name'=>'tipoUsuario',
            'value'=>Util::tipoUsuario($model->tipoUsuario),
        ),
	),
)); ?>
