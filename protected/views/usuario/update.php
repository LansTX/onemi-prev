<?php

$this->menu=array(
	array('label'=>'Listar Usuario','url'=>array('index')),
	array('label'=>'Nuevo Usuario','url'=>array('create')),
	array('label'=>'Ver Usuario','url'=>array('view','id'=>$model->idUsuario)),
	//array('label'=>'Administrar Usuario','url'=>array('admin')),
);
?>

<h1>Editando <?php echo $model->nombreUsuario; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>