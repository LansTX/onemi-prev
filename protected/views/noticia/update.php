<?php
$this->breadcrumbs=array(
	'Noticias'=>array('index'),
	$model->idNoticia=>array('view','id'=>$model->idNoticia),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Noticia','url'=>array('index')),
	array('label'=>'Crear Noticia','url'=>array('create')),
	array('label'=>'Ver Noticia','url'=>array('view','id'=>$model->idNoticia)),
	array('label'=>'Administrar Noticia','url'=>array('admin')),
);
?>

<h1>Editando Noticia: <?php echo $model->tituloNoticia; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>