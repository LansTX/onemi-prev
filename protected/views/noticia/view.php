<?php
$this->breadcrumbs=array(
	'Noticias'=>array('index'),
	$model->idNoticia,
);

$this->menu=array(
	array('label'=>'Listar Noticia','url'=>array('index')),
	array('label'=>'Crear Noticia','url'=>array('create')),
	array('label'=>'Modificar Noticia','url'=>array('update','id'=>$model->idNoticia)),
	array('label'=>'Eliminar Noticia','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idNoticia),'confirm'=>'Esta seguro de eliminar este item?')),
	//array('label'=>'Administrar Noticia','url'=>array('admin')),
);
?>

<h1>Viendo Noticia: <?php echo $model->tituloNoticia; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		//'idNoticia',
		'tituloNoticia',
		'subtituloNoticia',
		'fechaNoticia'=>array(
			'name'=>'fechaNoticia',
			'value'=>Util::convertirFechaTexto($model->fechaNoticia),
		),
		'cuerpoNoticia'=>array(
			'name'=>'cuerpoNoticia',
			'type'=>'raw',
			//'value'=>'<pre class="viewContentNoticia">'.$model->cuerpoNoticia.'</pre>',
			'value'=>'<div class="viewContentNoticia" align="justify">'.$model->cuerpoNoticia.'</div>',
			),
		'imagenNoticia'=>array(        
           	'name'=>'imagenNoticia',
           	'type'=>'raw',
            'value'=>  CHtml::image(Yii::app()->request->baseUrl.'/archivos/imagenes/'.$model->imagenNoticia,"imagenNoticia",array("width"=>'100%')),
        ),
		
	),
)); ?>
