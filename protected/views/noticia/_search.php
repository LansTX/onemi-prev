<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'idNoticia',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tituloNoticia',array('class'=>'span5','maxlength'=>300)); ?>

	<?php echo $form->textFieldRow($model,'subtituloNoticia',array('class'=>'span5','maxlength'=>500)); ?>

	<?php echo $form->textFieldRow($model,'fechaNoticia',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'cuerpoNoticia',array('class'=>'span5','maxlength'=>15000)); ?>

	<?php echo $form->textFieldRow($model,'imagenNoticia',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'Usuario_idUsuario',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
