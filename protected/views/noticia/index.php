<?php

$this->menu=array(
	array('label'=>'Nueva Noticia','url'=>array('create')),
	//array('label'=>'Administrar Alerta','url'=>array('admin')),
);
?>

<h1>Noticias</h1>
<div class="coloradd icon-block">
<a href="<?php echo Yii::app()->createUrl('noticia/create',array());?>">
<img class="mas" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/mas.png">
<span>Nuevo</span>
</a>
</div>
<table class="responstable" style="margin-top: 60px;">
	<thead>
		<tr>
			<th>Titulo</th>
			<th>SubTitulo</th>
			<th>Fecha de Creación</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$lista = Noticia::model()->findAll();
	if($lista)
	{
		foreach ($lista as $Item) {
			echo '<tr>';
			echo '<td>';
			echo $Item->tituloNoticia;
			echo '</td>';
			echo '<td>';
			echo $Item->subtituloNoticia;
			echo '</td>';
			echo '<td>';
			echo $Item->fechaNoticia;
			echo '</td>';			
			echo '<td class="opcionestable">';
			echo '<a href="'.Yii::app()->createUrl('noticia/view',array('id'=>$Item->idNoticia)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/ver.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('noticia/update',array('id'=>$Item->idNoticia)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/editar.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('noticia/delete',array('id'=>$Item->idNoticia)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/borrar.png"></a>';
			echo '<i  aria-hidden="true"></i>';
			echo '</td>';
			echo '</tr>';
		}
	}
	?>
	
		
	</tbody>
</table>





<?php 

//$this->widget('bootstrap.widgets.TbListView',array(	'dataProvider'=>$dataProvider,	'itemView'=>'_view',)); 
?>
