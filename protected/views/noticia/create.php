<?php
$this->breadcrumbs=array(
	'Noticias'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Noticia','url'=>array('index')),
	array('label'=>'Administrar Noticia','url'=>array('admin')),
);
?>

<h1>Nueva Noticia</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>