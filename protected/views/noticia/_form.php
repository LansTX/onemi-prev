<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'noticia-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="help-block">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'tituloNoticia',array('class'=>'span8','maxlength'=>300)); ?>

	<?php echo $form->textFieldRow($model,'subtituloNoticia',array('class'=>'span8','maxlength'=>500)); ?>

	<?php 
		//echo $form->textFieldRow($model,'imagenNoticia',array('class'=>'span5','maxlength'=>45)); 
	?>

	<?php echo $form->labelEx($model,'imagenNoticia'); ?>
	<i class="fa fa-upload" aria-hidden="true"></i>

    <?php echo CHtml::activeFileField($model, 'imagenNoticia'); ?>
    <?php echo $form->error($model,'imagenNoticia'); ?>

    <br/>
	<?php if($model->isNewRecord!='1'){ ?>
		<div>
     	<?php echo CHtml::image(Yii::app()->request->baseUrl.'/archivos/imagenes/'.$model->imagenNoticia,"imagenNoticia",array("width"=>200)); ?>
	</div>

	<?php } ?>

	<?php 
		$hoy = date("Y-m-d"); 
		echo $form->hiddenField($model,'fechaNoticia',array('value'=>$hoy)); ?>

<br/><br/>
	<?php echo $form->textAreaRow($model,'cuerpoNoticia',array('class'=>'textAreaCont','maxlength'=>15000)); ?>



	<?php echo $form->hiddenField($model,'Usuario_idUsuario',array('value'=>Yii::app()->user->getId())); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			'htmlOptions'=>array(
				'class'=>'botonForm',
				),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
<?php
$baseUrl = Yii::app()->theme->baseUrl;; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/tinymce/tinymce.min.js');
?>

<script>
	tinymce.init({ 
			selector:'textarea', 
			language:'es_MX', 
			plugins: 'lists wordcount insertdatetime preview',
			elementpath: false,
			fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
			toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | fontsizeselect"
	});
</script>