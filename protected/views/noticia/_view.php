<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idNoticia')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idNoticia),array('view','id'=>$data->idNoticia)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tituloNoticia')); ?>:</b>
	<?php echo CHtml::encode($data->tituloNoticia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subtituloNoticia')); ?>:</b>
	<?php echo CHtml::encode($data->subtituloNoticia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaNoticia')); ?>:</b>
	<?php echo CHtml::encode($data->fechaNoticia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cuerpoNoticia')); ?>:</b>
	<?php echo CHtml::encode($data->cuerpoNoticia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagenNoticia')); ?>:</b>
	<?php echo CHtml::encode($data->imagenNoticia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Usuario_idUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->Usuario_idUsuario); ?>
	<br />


</div>