<?php
$this->breadcrumbs=array(
	'Capacitacions'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Capacitación','url'=>array('index')),
	//array('label'=>'Administrar Capacitacion','url'=>array('admin')),
);
?>

<h1>Nueva Capacitación</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>