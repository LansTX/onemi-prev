<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'idCapacitacion',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'descripcionCap',array('class'=>'span5','maxlength'=>15000)); ?>

	<?php echo $form->textFieldRow($model,'tituloCap',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'imagenCap',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'Usuario_idUsuario',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'fechaCap',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'estadoCap',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'fechaLimCap',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
