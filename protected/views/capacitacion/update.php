<?php
$this->breadcrumbs=array(
	'Capacitacions'=>array('index'),
	$model->idCapacitacion=>array('view','id'=>$model->idCapacitacion),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Capacitacion','url'=>array('index')),
	array('label'=>'Crear Capacitacion','url'=>array('create')),
	//array('label'=>'Ver Capacitacion','url'=>array('view','id'=>$model->idCapacitacion)),
	//array('label'=>'Administrar Capacitacion','url'=>array('admin')),
);
?>

<h1>Editar Capacitación <?php echo $model->tituloCap; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>