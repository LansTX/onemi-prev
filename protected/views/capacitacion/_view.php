<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCapacitacion')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idCapacitacion),array('view','id'=>$data->idCapacitacion)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcionCap')); ?>:</b>
	<?php echo CHtml::encode($data->descripcionCap); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tituloCap')); ?>:</b>
	<?php echo CHtml::encode($data->tituloCap); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagenCap')); ?>:</b>
	<?php echo CHtml::encode($data->imagenCap); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Usuario_idUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->Usuario_idUsuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaCap')); ?>:</b>
	<?php echo CHtml::encode($data->fechaCap); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estadoCap')); ?>:</b>
	<?php echo CHtml::encode($data->estadoCap); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaLimCap')); ?>:</b>
	<?php echo CHtml::encode($data->fechaLimCap); ?>
	<br />

	*/ ?>

</div>