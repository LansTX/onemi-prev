<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'capacitacion-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="help-block">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php //echo $form->textFieldRow($model,'descripcionCap',array('class'=>'span5','maxlength'=>15000)); ?>

	<?php echo $form->textFieldRow($model,'tituloCap',array('class'=>'span5','maxlength'=>45)); ?>

	<?php //echo $form->textFieldRow($model,'imagenCap',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->hiddenField($model,'Usuario_idUsuario',array('value'=>Yii::app()->user->getId())); ?>

	<?php //echo $form->textFieldRow($model,'fechaCap',array('class'=>'span5')); ?>

	<?php 
		$AnioActual = date('Y');
		echo $form->labelEx($model,'fechaCap');
		$this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
	    'model'=>$model,
	    'attribute'=>'fechaCap',
	    'language' => 'es',
	    'options'=>array(
	    	'showAnim'=>'slide',
	        'dateFormat'=>'yy-mm-dd',
			'changeMonth'=>true,
	        'changeYear'=>true,
			'yearRange'=>($AnioActual-100).':'.$AnioActual,
			'showHour'=> false,
			'showMinute'=>false,
			'showTime'=>false,
			'timeFormat'=>'',
	        ),
	    )); 
	 ?>

	<?php //echo $form->textFieldRow($model,'estadoCap',array('class'=>'span5')); ?>

	<?php //<?php echo $form->textFieldRow($model,'estadoAlerta',array('class'=>'span5','maxlength'=>45)); 
		$seleccionado = array();
		if(!$model->isNewRecord){
			$seleccionado = array($model->estadoCap=>array('selected'=>true));
		}
		
		$Pregunta = "Escoja el estado de la Capacitación";
		$ListaOpciones = array('0'=>"Activado", '1'=>"Desactivado");

		echo $form->dropDownListRow($model,'estadoCap',$ListaOpciones,array('options'=>$seleccionado,'class'=>'span5', 'prompt'=>$Pregunta));
		

	?>

	<?php //echo $form->textFieldRow($model,'fechaLimCap',array('class'=>'span5')); ?>

	<?php 
		$AnioActual = date('Y');
		echo $form->labelEx($model,'fechaLimCap');
		$this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
	    'model'=>$model,
	    'attribute'=>'fechaLimCap',
	    'language' => 'es',
	    'options'=>array(
	    	'showAnim'=>'slide',
	        'dateFormat'=>'yy-mm-dd',
			'changeMonth'=>true,
	        'changeYear'=>true,
			'yearRange'=>($AnioActual-100).':'.$AnioActual,
			'showHour'=> false,
			'showMinute'=>false,
			'showTime'=>false,
			'timeFormat'=>'',
	        ),
	    )); 
	 ?>

	<?php echo $form->textAreaRow($model,'descripcionCap',array('class'=>'textAreaCont','maxlength'=>15000)); ?>

	<?php echo $form->labelEx($model,'imagenCap'); ?>
	<i class="fa fa-upload" aria-hidden="true"></i>

    <?php echo CHtml::activeFileField($model, 'imagenCap'); ?>
    <?php echo $form->error($model,'imagenCap'); ?>

    <br/>
	<?php if($model->isNewRecord!='1'){ ?>
		<div>
     	<?php echo CHtml::image(Yii::app()->request->baseUrl.'/archivos/imagenes/'.$model->imagenCap,"imagenCap",array("width"=>200)); ?>
	</div>

	<?php } ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			'htmlOptions'=>array(
				'class'=>'botonForm',
				),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
<?php
$baseUrl = Yii::app()->theme->baseUrl;; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/tinymce/tinymce.min.js');
?>

<script>
	tinymce.init({ 
			selector:'textarea', 
			language:'es_MX', 
			plugins: 'lists wordcount insertdatetime preview',
			elementpath: false,
			fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
			toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | fontsizeselect"
	});
	 document.getElementById('message_ifr').removeAttribute('title');
</script>