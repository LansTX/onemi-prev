<?php

$this->menu=array(
	array('label'=>'Nuevo Documento','url'=>array('create')),
	//array('label'=>'Administrar Alerta','url'=>array('admin')),
);
?>

<h1>Capacitaciones</h1>
<div class="coloradd icon-block">
<a href="<?php echo Yii::app()->createUrl('capacitacion/create',array());?>">
<img class="mas" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/mas.png">
<span>Nuevo</span>
</a>
</div>
<table class="responstable" style="margin-top: 60px;">
	<thead>
		<tr>
			<th>Título</th>
			<th>Fecha</th>
			<th>Fecha Límite</th>
			<th>Estado</th>
			<th>Acciones</th>
			<th>Lista de Inscritos</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$lista = Capacitacion::model()->findAll();
	if($lista)
	{
		foreach ($lista as $Item) {
			echo '<tr>';
			echo '<td>';
			echo $Item->tituloCap;
			echo '</td>';
			echo '<td>';
			echo $Item->fechaCap;
			echo '</td>';
			echo '<td>';
			echo $Item->fechaLimCap;
			echo '</td>';
			echo '<td>';
			echo $Item->estadoCap ? "Inactivo": "Activo";
			echo '</td>';
			
			
			echo '<td class="opcionestable">';
			echo '<a href="'.Yii::app()->createUrl('capacitacion/view',array('id'=>$Item->idCapacitacion)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/ver.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('capacitacion/update',array('id'=>$Item->idCapacitacion)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/editar.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('capacitacion/delete',array('id'=>$Item->idCapacitacion)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/borrar.png"></a>';
			echo '<i  aria-hidden="true"></i>';
			
			
			echo '<td>';
			//<img class="imagenView" src="/onemi-prev/themes/bootstrap/img/ver.png">
			/*
			echo '<a href="'.Yii::app()->createUrl('formcapacitacion/index2',array('id'=>$Item->idCapacitacion)).'"><i class="fa fa-user-plus fa-2x" aria-hidden="true"></i></a>';
			*/
			echo '<a href="'.Yii::app()->createUrl('formcapacitacion/index2',array('id'=>$Item->idCapacitacion)).'"><img class="imagenView" src="/onemi-prev/themes/bootstrap/img/mas.png"></a>';
			echo '</td>';
			echo '</tr>';
		}
	}
	?>
	
		
	</tbody>
</table>





<?php 

//$this->widget('bootstrap.widgets.TbListView',array(	'dataProvider'=>$dataProvider,	'itemView'=>'_view',)); 
?>
