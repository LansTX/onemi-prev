<?php
$this->breadcrumbs=array(
	'Capacitacions'=>array('index'),
	$model->idCapacitacion,
);

$this->menu=array(
	array('label'=>'Listar Capacitacion','url'=>array('index')),
	array('label'=>'Crear Capacitacion','url'=>array('create')),
	array('label'=>'Modificar Capacitacion','url'=>array('update','id'=>$model->idCapacitacion)),
	array('label'=>'Eliminar Capacitacion','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idCapacitacion),'confirm'=>'Esta seguro de eliminar este item?')),
	//array('label'=>'Administrar Capacitacion','url'=>array('admin')),
);
?>

<h1>Capacitación: <?php echo $model->tituloCap; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		//'idCapacitacion',
		
		'tituloCap',
		
		'Usuario_idUsuario'=>array(
			'name'=>'Usuario_idUsuario',
			'value'=>Usuario::getNombreUsuario($model->Usuario_idUsuario),
			),
		'fechaCap'=>array(
			'name'=>'fechaCap',
			'value'=>Util::convertirFechaTexto($model->fechaCap),
		),
		
		'fechaLimCap'=>array(
			'name'=>'fechaLimCap',
			'value'=>Util::convertirFechaTexto($model->fechaLimCap),
		),
		'estadoCap'=>array(
			'name'=>'estadoCap',
			'value'=> $model->estadoCap ? "Inactivo": "Activo",
			),
		
		'descripcionCap'=>array(
			'name'=>'descripcionCap',
			'type'=>'raw',
			//'value'=>'<pre class="viewContentNoticia">'.$model->cuerpoNoticia.'</pre>',
			'value'=>'<div class="viewContentNoticia" align="justify">'.$model->descripcionCap.'</div>',
			),
		'imagenCap'=>array(        
           	'name'=>'imagenCap',
           	'type'=>'raw',
            'value'=>  CHtml::image(Yii::app()->request->baseUrl.'/archivos/imagenes/'.$model->imagenCap,"imagenCap",array("width"=>'100%')),
        ),
	),
)); ?>
