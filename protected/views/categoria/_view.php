﻿<div class="view">

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$data,
	'attributes'=>array(
		array(
			'label'=>'Link',
			'name'=>'Link',
			'type'=>'raw',
			'value'=>CHtml::link("Ver Categoría",Yii::app()->createUrl("categoria/view/".CHtml::encode($data->idCategoria))),
			),
		'nombre',
		array(
			'label'=>'Zonas',
			'name'=>'',
			'type'=>'raw',
			'value'=>CHtml::link("Zonas Seguras",Yii::app()->createUrl("zonasegura/listazonacategoria/".CHtml::encode($data->idCategoria))),
			),
	),
)); ?>


</div>