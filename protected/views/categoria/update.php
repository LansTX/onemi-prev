<?php
$this->breadcrumbs=array(
	'Categorias'=>array('index'),
	$model->idCategoria=>array('view','id'=>$model->idCategoria),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Categoría','url'=>array('index')),
	array('label'=>'Nueva Categoría','url'=>array('create')),
	array('label'=>'Ver Categoría','url'=>array('view','id'=>$model->idCategoria)),
	//array('label'=>'Administrar Categoria','url'=>array('admin')),
);
?>

<h1>Editar Categoria: <?php echo $model->nombreCategoria; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>