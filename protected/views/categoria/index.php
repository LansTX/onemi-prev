<?php
$this->breadcrumbs=array(
	'Categorias',
);

$this->menu=array(
	array('label'=>'Nueva Categoría','url'=>array('create')),
	//array('label'=>'Administrar Categoria','url'=>array('admin')),
);
?>

<h1>Categorías</h1>
<div class="coloradd icon-block">
<a href="<?php echo Yii::app()->createUrl('categoria/create',array());?>">
<img class="mas" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/mas.png">
<span>Nuevo</span>
</a>
</div>
<table class="responstable" style="margin-top: 60px;">
	<thead>
		<tr>
			<th>Nombre de Categoría</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$lista = Categoria::model()->findAll();
	if($lista)
	{
		foreach ($lista as $Item) {
			echo '<tr>';
			echo '<td>';
			echo $Item->nombreCategoria;
			echo '</td>';
			
			
			echo '<td class="opcionestable">';
			echo '<a href="'.Yii::app()->createUrl('categoria/view',array('id'=>$Item->idCategoria)).'"><img class="imagenView" src="/onemi-prev/themes/bootstrap/img/ver.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('categoria/update',array('id'=>$Item->idCategoria)).'"><img class="imagenView" src="/onemi-prev/themes/bootstrap/img/editar.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('categoria/delete',array('id'=>$Item->idCategoria)).'"><img class="imagenView" src="/onemi-prev/themes/bootstrap/img/borrar.png"></a>';
			echo '<i  aria-hidden="true"></i>';
			echo '</td>';
			echo '</tr>';
		}
	}
	?>
	
		
	</tbody>
</table>



<?php 

//$this->widget('bootstrap.widgets.TbListView',array(	'dataProvider'=>$dataProvider,'itemView'=>'_view',)); 
?>
