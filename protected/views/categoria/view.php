﻿<?php
$this->breadcrumbs=array(
	'Categorias'=>array('index'),
	$model->idCategoria,
);

$this->menu=array(
	array('label'=>'Listar Categoria','url'=>array('index')),
	array('label'=>'Crear Categoria','url'=>array('create')),
	array('label'=>'Modificar Categoria','url'=>array('update','id'=>$model->idCategoria)),
	array('label'=>'Eliminar Categoria','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idCategoria),'confirm'=>'Esta seguro de eliminar este item?')),
	array('label'=>'Administrar Categoria','url'=>array('admin')),
);
?>

<h1>Categoría: <?php echo $model->nombreCategoria; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		//'idcategoria',
		'nombreCategoria',
	),
)); ?>
