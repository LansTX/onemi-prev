<?php
$this->breadcrumbs=array(
	'Rutas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Ruta','url'=>array('index')),
	//array('label'=>'Administrar Ruta','url'=>array('admin')),
);
?>

<h1>Nueva Ruta</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>