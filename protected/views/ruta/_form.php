<div style="width:100%">
	<div id="mapdiv" style="width:100%; height:600px;">
        
    </div>

	<script type="text/javascript">
	  	var map;
	  	var created = false;
	  	var lastmarker;

	  	function mapa()

	  	{

	    //var opts = {'center': new google.maps.LatLng(26.12295, -80.17122), 'zoom':11, 'mapTypeId': google.maps.MapTypeId.ROADMAP }
	   	var opts = {'center': new google.maps.LatLng(-18.487171, -70.306298), 'zoom':13, 'mapTypeId': google.maps.MapTypeId.ROADMAP }
	   	map = new google.maps.Map(document.getElementById('mapdiv'),opts);
	   	var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';

	   	var camino = new google.maps.MVCArray();
	   	<?php if (!$model->isNewRecord): ?>

	   		var camino = google.maps.geometry.encoding.decodePath("<?php echo $model->textoRuta?>");
	   		var caminoOriginal = camino;

	   	<?php endif;?>



	   	var optpoly = {
	   		path: camino,
	   		strokeColor: "#55acee", strokeWeight: 5
	   	};

	   	var polilinea = new google.maps.Polyline(optpoly);

	   	polilinea.setMap(map);



	    google.maps.event.addListener(map,'click',function(event) {

	    	var camino = polilinea.getPath();
	    	
	    	var lat = event.latLng.lat();
	    	var long = event.latLng.lng();

	    	camino.push ( event.latLng);

	    	//console.log(camino.getArray().toString());


	    	var string = google.maps.geometry.encoding.encodePath(camino).replace(/\\/g,"\\\\");

	    	console.log(string);

	    	document.getElementById('Ruta_textoRuta').value = string; 
	    	//document.getElementById('ZonaSegura_latitudZS').value = str.toString().substring(0, 11);



	   	});

  		$("#delete").click(function(){
    		var run = polilinea.getPath();
    		run.clear();
    		document.getElementById('Ruta_textoRuta').value = "";

		});

  		<?php if (!$model->isNewRecord): ?>

	   		var bounds = new google.maps.LatLngBounds();
    		var points = polilinea.getPath().getArray();
    		for (var n = 0; n < points.length ; n++){
        		bounds.extend(points[n]);
    		}
    		map.fitBounds(bounds);

  			$("#reset").click(function(){
    			var run = polilinea.getPath();
    			run.clear();
    			var optpoly = {
	   				path: camino,
	   				strokeColor: "#55acee", strokeWeight: 5
	   			};
    			polilinea.setOptions(optpoly);
    			var string = google.maps.geometry.encoding.encodePath(camino);
    			document.getElementById('Ruta_textoRuta').value = string;
			});

	   	<?php endif;?>


	  	}


  	</script>
  	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9_FJ8r7K4pNJPfNLOrYsY1sk6fyEdtP0&libraries=geometry&callback=mapa" async defer></script>
</div>
<br/>
<div id="delete">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Reiniciar',
    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'large', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array(
    	'style'=>'display:inline-block',
    	'class'=>'botonForm',
    	),
)); ?>
</div>


<?php if (!$model->isNewRecord){
	echo '<div id="reset">';
	$this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Camino Original',
    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'large', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array(
    	'style'=>'display:inline-block',
    	'class'=>'botonForm',
    	),
)); 
	echo '</div>';
}
?>
<br/>



<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'ruta-form',
	'enableAjaxValidation'=>false,
)); ?>
<br/><br/>
	<p class="help-block">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nombreRuta',array('class'=>'span5','maxlength'=>45)); ?>

	<?php 
	$hoy = date("Y-m-d"); 
	echo $form->hiddenField($model,'fechaRuta',array('value'=>$hoy)); ?>

	<?php echo $form->hiddenField($model,'textoRuta',array('class'=>'span5','maxlength'=>50000)); ?>

	<?php echo $form->hiddenField($model,'usuario_idUsuario',array('value'=>Yii::app()->user->getId())); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			'htmlOptions'=>array(
				'class'=>'botonForm',
				),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
