<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idRuta')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idRuta),array('view','id'=>$data->idRuta)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombreRuta')); ?>:</b>
	<?php echo CHtml::encode($data->nombreRuta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaRuta')); ?>:</b>
	<?php echo CHtml::encode($data->fechaRuta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('textoRuta')); ?>:</b>
	<?php echo CHtml::encode($data->textoRuta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_idUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_idUsuario); ?>
	<br />


</div>