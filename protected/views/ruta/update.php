<?php
$this->breadcrumbs=array(
	'Rutas'=>array('index'),
	$model->idRuta=>array('view','id'=>$model->idRuta),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Ruta','url'=>array('index')),
	array('label'=>'Crear Ruta','url'=>array('create')),
	array('label'=>'Ver Ruta','url'=>array('view','id'=>$model->idRuta)),
	//array('label'=>'Administrar Ruta','url'=>array('admin')),
);
?>

<h1>Modificar Ruta <?php echo $model->idRuta; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>