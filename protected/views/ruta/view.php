<?php
$this->breadcrumbs=array(
	'Rutas'=>array('index'),
	$model->idRuta,
);

$this->menu=array(
	array('label'=>'Listar Ruta','url'=>array('index')),
	array('label'=>'Crear Ruta','url'=>array('create')),
	array('label'=>'Modificar Ruta','url'=>array('update','id'=>$model->idRuta)),
	array('label'=>'Eliminar Ruta','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idRuta),'confirm'=>'Esta seguro de eliminar este item?')),
	//array('label'=>'Administrar Ruta','url'=>array('admin')),
);
?>

<h1><?php echo $model->nombreRuta; ?></h1>


	<div style="width:90%; height:400px; margin: auto;" id="map">

	</div>

<script>
<?php 
echo "
	var map;
	function initMap() {

        var mapDiv = document.getElementById('map');

        //añadir mapa
        var opts = {'center': new google.maps.LatLng(-18.487171, -70.306298), 'zoom':13, 'mapTypeId': google.maps.MapTypeId.ROADMAP }
	   	map = new google.maps.Map(document.getElementById('map'),opts);
	   	
	   	var camino = google.maps.geometry.encoding.decodePath('".$model->textoRuta."');

	   	console.log(camino);
	    var optpoly = {
	    	editable: false,
	    	path: camino,
	   		strokeColor: '#55acee', strokeWeight: 5
	   	};

	   	var polilinea = new google.maps.Polyline(optpoly);

	   	polilinea.setMap(map);

	   	var bounds = new google.maps.LatLngBounds();
    	var points = polilinea.getPath().getArray();
    	for (var n = 0; n < points.length ; n++){
        	bounds.extend(points[n]);
    	}
    	map.fitBounds(bounds);
	    
	    //map.fitBounds(polilinea.getBounds());
}
        
	
       
";
?>
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9_FJ8r7K4pNJPfNLOrYsY1sk6fyEdtP0&libraries=geometry&callback=initMap" async defer></script>