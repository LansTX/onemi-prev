<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'documento-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="help-block">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php //echo $form->textFieldRow($model,'rutaDoc',array('class'=>'span5','maxlength'=>45)); ?>

	

    <?php if($model->isNewRecord!='1'){ ?>
    	
    <?php echo '<a style="color:black" href="'.Yii::app()->createUrl('documento/download',array('id'=>$model->idDocumento)).'">Descargar archivo <br/><i style="color:black" class="fa fa-download fa-2x" aria-hidden="true"></i></a>'; ?>
    <br/><br/>
    <?php } ?>


    <?php echo $form->labelEx($model,'rutaDoc'); ?>
	<i class="fa fa-upload" aria-hidden="true"></i>
    <?php echo CHtml::activeFileField($model, 'rutaDoc'); ?>

    <?php echo $form->error($model,'rutaDoc'); ?>
<br/><br/>
	<?php echo $form->textFieldRow($model,'nombreDoc',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->hiddenField($model,'sizeDoc',array('value'=>4000)); ?>

	<?php //echo $form->textFieldRow($model,'fechaDoc',array('class'=>'span5')); ?>

	<?php 
		$hoy = date("Y-m-d"); 
		echo $form->hiddenField($model,'fechaDoc',array('value'=>$hoy)); 
	?>

	<?php //echo $form->textFieldRow($model,'CategoriaDocumento_idCategoriaDocumento',array('class'=>'span5')); ?>

	<?php echo $form->dropdownlistrow($model,'CategoriaDocumento_idCategoriaDocumento',CHtml::listData( CategoriaDocumento::model()->findAll(), 'idCategoriaDocumento', 'nombreCatDoc'),array('class'=>'span3',"style"=>"margin-left: 5px;")); ?>

	<?php echo $form->hiddenField($model,'Usuario_idUsuario',array('value'=>Yii::app()->user->getId())); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			'htmlOptions'=>array(
				'class'=>'botonForm',
				),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
