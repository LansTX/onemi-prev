<?php
$this->breadcrumbs=array(
	'Documentos'=>array('index'),
	$model->idDocumento=>array('view','id'=>$model->idDocumento),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Documento','url'=>array('index')),
	array('label'=>'Crear Documento','url'=>array('create')),
	array('label'=>'Ver Documento','url'=>array('view','id'=>$model->idDocumento)),
	array('label'=>'Administrar Documento','url'=>array('admin')),
);
?>

<h1>Editar: <?php echo $model->nombreDoc; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>