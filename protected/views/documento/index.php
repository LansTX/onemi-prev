<?php

$this->menu=array(
	array('label'=>'Nuevo Documento','url'=>array('create')),
	array('label'=>'Mis documentos subidos','url'=>array('documentossubidos')),
	array('label'=>'Documentos Disponibles','url'=>array('index')),
	//array('label'=>'Administrar Alerta','url'=>array('admin')),
);
?>

<h1>Documentos Disponibles</h1>
<div class="coloradd icon-block">
<a href="<?php echo Yii::app()->createUrl('documento/create',array());?>">
<img class="mas" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/mas.png">
<span>Nuevo</span>
</a>
</div>
<table class="responstable" style="margin-top: 60px;">
	<thead>
		<tr>
			
			<th>Nombre Archivo</th>
			<th>Fecha de Subida</th>
			<!--<th>Subido por</th>-->
			<th>Categoría</th>
			<th>Acciones</th>
			<th>Descarga</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$lista = Documento::model()->findAll();
	if($lista)
	{
		foreach ($lista as $Item) {
			echo '<tr>';
			echo '<td>';
			echo $Item->nombreDoc;
			echo '</td>';
			/*echo '<td>';
			echo Usuario::getNombreUsuario($Item->Usuario_idUsuario);
			echo '</td>';*/
			echo '<td>';
			echo $Item->fechaDoc;
			echo '</td>';
			echo '<td>';
			echo CHtml::link(Documento::getNombreCategoria($Item->CategoriaDocumento_idCategoriaDocumento),Yii::app()->createUrl("categoriadocumento/view/".CHtml::encode($Item->CategoriaDocumento_idCategoriaDocumento)));
			echo '</td>';
			/*
			echo '<td>';
			echo $Item->sizeDoc;
			echo '</td>';
			*/
			echo '<td class="opcionestable">';
			echo '<a href="'.Yii::app()->createUrl('documento/view',array('id'=>$Item->idDocumento)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/ver.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('documento/update',array('id'=>$Item->idDocumento)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/editar.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('documento/delete',array('id'=>$Item->idDocumento)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/borrar.png"></a>';
			echo '<i  aria-hidden="true"></i>';
			
			echo '<td>';
			echo '<a href="'.Yii::app()->createUrl('documento/download',array('id'=>$Item->idDocumento)).'"><i style="color:black" class="fa fa-download fa-lg " aria-hidden="true"></i></a>';
			
			echo '</td>';
			echo '</tr>';
		}
	}
	?>
	
		
	</tbody>
</table>





<?php 

//$this->widget('bootstrap.widgets.TbListView',array(	'dataProvider'=>$dataProvider,	'itemView'=>'_view',)); 
?>
