<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'idDocumento',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'rutaDoc',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'nombreDoc',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'sizeDoc',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'fechaDoc',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'CategoriaDocumento_idCategoriaDocumento',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'Usuario_idUsuario',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
