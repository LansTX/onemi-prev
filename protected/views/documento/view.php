<?php
$this->breadcrumbs=array(
	'Documentos'=>array('index'),
	$model->idDocumento,
);

$this->menu=array(
	array('label'=>'Listar Documento','url'=>array('index')),
	array('label'=>'Crear Documento','url'=>array('create')),
	array('label'=>'Modificar Documento','url'=>array('update','id'=>$model->idDocumento)),
	array('label'=>'Eliminar Documento','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idDocumento),'confirm'=>'Esta seguro de eliminar este item?')),
	//array('label'=>'Administrar Documento','url'=>array('admin')),
);
?>

<h1><?php echo $model->nombreDoc; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'rutaDoc',
		'nombreDoc',
		'sizeDoc',
		'fechaDoc',
		'CategoriaDocumento_idCategoriaDocumento'=>array(
			'name'=>'CategoriaDocumento_idCategoriaDocumento',
			'value'=>Documento::getNombreCategoria($model->CategoriaDocumento_idCategoriaDocumento),
			),
		'Usuario_idUsuario'=>array(
			'name'=>'Usuario_idUsuario',
			'value'=>Usuario::getNombreUsuario($model->Usuario_idUsuario),
			),
		'custom'=>array(
			'name'=>'Descargar',
			'label'=>'Descargar',
			'type'=>'raw',
			'value'=>'<a style="color:black;" href="'.Yii::app()->createUrl('documento/download',array('id'=>$model->idDocumento)).'"><i class="fa fa-download fa-lg" aria-hidden="true"></i></a>',
			),
		
	),
)); ?>
