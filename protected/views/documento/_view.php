<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idDocumento')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idDocumento),array('view','id'=>$data->idDocumento)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rutaDoc')); ?>:</b>
	<?php echo CHtml::encode($data->rutaDoc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombreDoc')); ?>:</b>
	<?php echo CHtml::encode($data->nombreDoc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sizeDoc')); ?>:</b>
	<?php echo CHtml::encode($data->sizeDoc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaDoc')); ?>:</b>
	<?php echo CHtml::encode($data->fechaDoc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CategoriaDocumento_idCategoriaDocumento')); ?>:</b>
	<?php echo CHtml::encode($data->CategoriaDocumento_idCategoriaDocumento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Usuario_idUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->Usuario_idUsuario); ?>
	<br />


</div>