<?php
$this->breadcrumbs=array(
	'Documentos'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Documento','url'=>array('index')),
	array('label'=>'Mis documentos subidos','url'=>array('documentossubidos')),
	array('label'=>'Documentos Disponibles','url'=>array('index')),
	//array('label'=>'Administrar Documento','url'=>array('admin')),
);
?>

<h1>Nuevo Documento</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>