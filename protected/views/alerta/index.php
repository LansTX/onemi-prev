<?php
$this->breadcrumbs=array(
	'Alertas',
);

$this->menu=array(
	array('label'=>'Nueva Alerta','url'=>array('create')),
	//array('label'=>'Administrar Alerta','url'=>array('admin')),
);
?>

<h1>Alertas</h1>
<div class="coloradd icon-block">
<a href="<?php echo Yii::app()->createUrl('alerta/create',array());?>">
<img class="mas" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/mas.png">
<span>Nuevo</span>
</a>
</div>
<table class="responstable" style="margin-top: 60px;">
	<thead>
		<tr>
			<th>Mensaje de Alerta</th>
			<th>Estado</th>
			<th>Fecha</th>
			<th>Tipo de Alertas</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$lista = Alerta::model()->findAll();
	if($lista)
	{
		foreach ($lista as $Item) {
			echo '<tr>';
			echo '<td>';
			echo $Item->mensajeAlerta;
			echo '</td>';
			echo '<td>';
			echo $Item->estadoAlerta ? "Inactivo": "Activo";
			echo '</td>';
			echo '<td>';
			echo $Item->fechaAlerta;
			echo '</td>';			
			echo '<td>' ;
				if    ($Item->ordenAlerta == 0) echo '<img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/alertaverde.png">';
				elseif($Item->ordenAlerta == 1) echo '<img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/alertaverde.png">';
				elseif($Item->ordenAlerta == 2) echo '<img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/alertaamarilla.png">';
				elseif($Item->ordenAlerta == 3) echo '<img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/alertaroja.png">';
				else echo "Sin establecer";
				
			echo '</td>';
			echo '<td class="opcionestable">';
			echo '<a href="'.Yii::app()->createUrl('alerta/view',array('id'=>$Item->idAlerta)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/ver.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('alerta/update',array('id'=>$Item->idAlerta)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/editar.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('alerta/delete',array('id'=>$Item->idAlerta)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/borrar.png"></a>';
			echo '<i  aria-hidden="true"></i>';
	
			echo '</td>';
			echo '</tr>';
		}
	}
	?>
	
		
	</tbody>
</table>





<?php 

//$this->widget('bootstrap.widgets.TbListView',array(	'dataProvider'=>$dataProvider,	'itemView'=>'_view',)); 
?>
