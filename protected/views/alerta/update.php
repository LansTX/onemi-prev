<?php
$this->breadcrumbs=array(
	'Alertas'=>array('index'),
	$model->idAlerta=>array('view','id'=>$model->idAlerta),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Alerta','url'=>array('index')),
	array('label'=>'Nueva Alerta','url'=>array('create')),
	array('label'=>'Ver Alerta','url'=>array('view','id'=>$model->idAlerta)),
	//array('label'=>'Administrar Alerta','url'=>array('admin')),
);
?>

<h1>Editar Alerta <?php echo $model->fechaAlerta; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>