<?php

$this->menu=array(
	array('label'=>'Listar Alerta','url'=>array('index')),
	array('label'=>'Crear Alerta','url'=>array('create')),
	array('label'=>'Modificar Alerta','url'=>array('update','id'=>$model->idAlerta)),
	array('label'=>'Eliminar Alerta','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idAlerta),'confirm'=>'Esta seguro de eliminar este item?')),
	//array('label'=>'Administrar Alerta','url'=>array('admin')),
);
?>

<h1>Alerta <?php echo Util::convertirFechaTexto($model->fechaAlerta); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		//'idalerta',
		'mensajeAlerta',
		'estadoAlerta'=>array(
			'name'=>'estadoAlerta',
			'value'=> $model->estadoAlerta ? "Inactivo": "Activo",
			),
		'fechaAlerta',
		'imagenAlerta'=>array(        
           	'name'=>'imagenAlerta',
           	'type'=>'raw',
            'value'=>  CHtml::image(Yii::app()->request->baseUrl.'/archivos/imagenes/'.$model->imagenAlerta,"imagenAlerta",array("width"=>'100%')),
        ),
        'ordenAlerta'=>array(
			'name'=>'ordenAlerta',
			'value'=> function ($model){
				if ($model->ordenAlerta == 0) return "Alerta Verde";
				elseif($model->ordenAlerta == 1) return "Alerta Temprana Preventiva";
				elseif($model->ordenAlerta == 2) return "Alerta Amarilla";
				elseif($model->ordenAlerta == 3) return "Alerta Roja";
				else return "No especificado";
			}, 
			),
		//'usuario_idUsuario',
	),
)); ?>
