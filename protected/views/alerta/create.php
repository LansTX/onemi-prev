<?php
$this->breadcrumbs=array(
	'Alertas'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar Alerta','url'=>array('index')),
	//array('label'=>'Administrar Alerta','url'=>array('admin')),
);
?>

<h1>Nueva Alerta</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>