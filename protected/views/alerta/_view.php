<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idAlerta')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idAlerta),array('view','id'=>$data->idAlerta)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mensajeAlerta')); ?>:</b>
	<?php echo CHtml::encode($data->mensajeAlerta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imagenAlerta')); ?>:</b>
	<?php echo CHtml::encode($data->imagenAlerta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estadoAlerta')); ?>:</b>
	<?php echo CHtml::encode($data->estadoAlerta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaAlerta')); ?>:</b>
	<?php echo CHtml::encode($data->fechaAlerta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ordenAlerta')); ?>:</b>
	<?php echo CHtml::encode($data->ordenAlerta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Usuario_idUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->Usuario_idUsuario); ?>
	<br />


</div>