<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'alerta-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="help-block">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textAreaRow($model,'mensajeAlerta',array('class'=>'span6','maxlength'=>5000)); ?>

	<?php //echo $form->textFieldRow($model,'imagenAlerta',array('class'=>'span3','maxlength'=>45)); ?>

	<?php echo $form->labelEx($model,'imagenAlerta'); ?>
	<i class="fa fa-upload" aria-hidden="true"></i>
    <?php echo CHtml::activeFileField($model, 'imagenAlerta'); ?>
    <?php echo $form->error($model,'imagenAlerta'); ?>
<br/>
    <?php if($model->isNewRecord!='1'){ ?>

     <?php echo CHtml::image(Yii::app()->request->baseUrl.'/archivos/imagenes/'.$model->imagenAlerta,"imagenAlerta",array("width"=>200)); ?>

    <?php } ?>
	<br/>
	<?php //<?php echo $form->textFieldRow($model,'estadoAlerta',array('class'=>'span5','maxlength'=>45)); 
		$seleccionado = array();
		if(!$model->isNewRecord){
			$seleccionado = array($model->estadoAlerta=>array('selected'=>true));
		}
		
		$Pregunta = "Escoja el estado de la Alerta";
		$ListaOpciones = array('0'=>"Activo", '1'=>"Desactivado");

		echo $form->dropDownListRow($model,'estadoAlerta',$ListaOpciones,array('options'=>$seleccionado,'class'=>'span5', 'prompt'=>$Pregunta));
		

	?><br/>
<?php 
	$AnioActual = date('Y');
	echo $form->labelEx($model,'fechaAlerta');
	$this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
    'model'=>$model,
    'attribute'=>'fechaAlerta',
    'language' => 'es',
    'options'=>array(
    	'showAnim'=>'slide',
        'dateFormat'=>'yy-mm-dd',
		'changeMonth'=>true,
        'changeYear'=>true,
		'yearRange'=>($AnioActual-100).':'.$AnioActual,
		'showHour'=> false,
		'showMinute'=>false,
		'showTime'=>false,
		'timeFormat'=>'',
        ),
    )); 
 ?>

	<?php echo $form->hiddenField($model,'Usuario_idUsuario',array('class'=>'span3','value'=>Yii::app()->user->getId()  )); ?>

	<?php //echo $form->textFieldRow($model,'ordenAlerta',array('class'=>'span5','maxlength'=>45)); 

		$seleccionado = array();
		if(!$model->isNewRecord){
			$seleccionado = array($model->ordenAlerta=>array('selected'=>true));
		}
		
		$Pregunta = "Escoja el tipo de Alerta";
		$ListaOpciones = array('0'=>"Alerta Verde", '1'=>"Alerta Temprana Preventiva", '2'=>"Alerta Amarilla",'3'=>'Alerta Roja');

		echo $form->dropDownListRow($model,'ordenAlerta',$ListaOpciones,array('options'=>$seleccionado,'class'=>'span5', 'prompt'=>$Pregunta));
		

	

	?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			'htmlOptions'=>array(
				'class'=>'botonForm',
				),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
