<?php
$this->breadcrumbs=array(
	'Form Capacitacions'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar FormCapacitacion','url'=>array('index')),
	//array('label'=>'Administrar FormCapacitacion','url'=>array('admin')),
);
?>

<h1>Nuevo Formulario</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>