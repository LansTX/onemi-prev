<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'form-capacitacion-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nombreForm',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'apellidoForm',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'emailForm',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'celularForm',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'razonForm',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'cargoForm',array('class'=>'span5','maxlength'=>45)); ?>

	<?php //echo $form->textFieldRow($model,'Capacitacion_idCapacitacion',array('class'=>'span5')); ?>

	<?php echo $form->dropdownlistrow($model,'Capacitacion_idCapacitacion',CHtml::listData( Capacitacion::model()->findAll(), 'idCapacitacion', 'tituloCap'),array('class'=>'span3',"style"=>"margin-left: 5px;")); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			'htmlOptions'=>array(
				'class'=>'botonForm',
				),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
