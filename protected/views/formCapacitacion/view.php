<?php
$this->breadcrumbs=array(
	'Form Capacitacions'=>array('index'),
	$model->idFormCapacitacion,
);

$this->menu=array(
	array('label'=>'Listar FormCapacitacion','url'=>array('index')),
	array('label'=>'Crear FormCapacitacion','url'=>array('create')),
	array('label'=>'Modificar FormCapacitacion','url'=>array('update','id'=>$model->idFormCapacitacion)),
	array('label'=>'Eliminar FormCapacitacion','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idFormCapacitacion),'confirm'=>'Esta seguro de eliminar este item?')),
	//array('label'=>'Administrar FormCapacitacion','url'=>array('admin')),
);
?>

<h1>Viendo Formulario <?php echo $model->nombreForm; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		//'idFormCapacitacion',
		'nombreForm',
		'apellidoForm',
		'emailForm',
		'celularForm',
		'razonForm',
		'cargoForm',
		/*
		'Capacitacion_idCapacitacion'=>array(
			'name'=>'Capacitacion_idCapacitacion',
			'value'=>0,
		),*/
))); ?>
