<?php
$this->breadcrumbs=array(
	'Form Capacitacions',
);

$this->menu=array(
	array('label'=>'Crear FormCapacitacion','url'=>array('create')),
	array('label'=>'Administrar FormCapacitacion','url'=>array('admin')),
);
?>

<h1>Form Capacitacions</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
