<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'idFormCapacitacion',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nombreForm',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'apellidoForm',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'emailForm',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'celularForm',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'razonForm',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'cargoForm',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'Capacitacion_idCapacitacion',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
