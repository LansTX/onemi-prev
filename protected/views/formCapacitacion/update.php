<?php
$this->breadcrumbs=array(
	'Form Capacitacions'=>array('index'),
	$model->idFormCapacitacion=>array('view','id'=>$model->idFormCapacitacion),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar FormCapacitacion','url'=>array('index')),
	array('label'=>'Crear FormCapacitacion','url'=>array('create')),
	array('label'=>'Ver FormCapacitacion','url'=>array('view','id'=>$model->idFormCapacitacion)),
	//array('label'=>'Administrar FormCapacitacion','url'=>array('admin')),
);
?>

<h1>Editar Formulario <?php echo $model->nombreForm; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>