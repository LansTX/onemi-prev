<?php

$this->menu=array(
	array('label'=>'Nueva Inscripción','url'=>array('create')),
	//array('label'=>'Administrar Alerta','url'=>array('admin')),
);
?>

<h1>Lista de Inscritos</h1>
<div class="coloradd icon-block">
<a href="<?php echo Yii::app()->createUrl('formcapacitacion/create',array());?>">
<img class="mas" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/mas.png">
<span>Nuevo</span>
</a>
</div>
<table class="responstable" style="margin-top: 60px;">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>Email</th>
			<th>Celular</th>
			<th>Razón</th>
			<th>Cargo</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$lista = FormCapacitacion::model()->findAll($criteria);
	if($lista)
	{
		foreach ($lista as $Item) {
			echo '<tr>';
			echo '<td>';
			echo $Item->nombreForm;
			echo '</td>';
			echo '<td>';
			echo $Item->apellidoForm;
			echo '</td>';
			echo '<td>';
			echo $Item->emailForm;
			echo '</td>';
			echo '<td>';
			echo $Item->celularForm;
			echo '</td>';
			echo '<td>';
			echo $Item->razonForm;
			echo '</td>';
			echo '<td>';
			echo $Item->cargoForm;
			echo '</td>';
			
			
			echo '<td class="opcionestable">';
			echo '<a href="'.Yii::app()->createUrl('formcapacitacion/view',array('id'=>$Item->idFormCapacitacion)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/ver.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('formcapacitacion/update',array('id'=>$Item->idFormCapacitacion)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/editar.png"></a>';
			echo '<a href="'.Yii::app()->createUrl('formcapacitacion/delete',array('id'=>$Item->idFormCapacitacion)).'"><img class="imagenView" src="'.Yii::app()->theme->baseUrl.'/img/borrar.png"></a>';
			echo '<i  aria-hidden="true"></i>';
			
			/*
			echo '<td>';
			echo '<a href="'.Yii::app()->createUrl('formcapacitacion/index2',array('id'=>$Item->idCapacitacion)).'"><i class="fa fa-user-plus fa-2x" aria-hidden="true"></i></a>';
			*/
			echo '</td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td colspan="7">No se encontraron resultados.</td></tr>';
	}
	?>
	
		
	</tbody>
</table>





<?php 

//$this->widget('bootstrap.widgets.TbListView',array(	'dataProvider'=>$dataProvider,	'itemView'=>'_view',)); 
?>
