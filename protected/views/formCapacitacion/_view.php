<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idFormCapacitacion')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idFormCapacitacion),array('view','id'=>$data->idFormCapacitacion)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombreForm')); ?>:</b>
	<?php echo CHtml::encode($data->nombreForm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellidoForm')); ?>:</b>
	<?php echo CHtml::encode($data->apellidoForm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emailForm')); ?>:</b>
	<?php echo CHtml::encode($data->emailForm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('celularForm')); ?>:</b>
	<?php echo CHtml::encode($data->celularForm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('razonForm')); ?>:</b>
	<?php echo CHtml::encode($data->razonForm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cargoForm')); ?>:</b>
	<?php echo CHtml::encode($data->cargoForm); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Capacitacion_idCapacitacion')); ?>:</b>
	<?php echo CHtml::encode($data->Capacitacion_idCapacitacion); ?>
	<br />

	*/ ?>

</div>