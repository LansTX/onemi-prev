<?php
$this->breadcrumbs=array(
	'Categoria Documentos'=>array('index'),
	$model->idCategoriaDocumento,
);

$this->menu=array(
	array('label'=>'Listar Categoria Documento','url'=>array('index')),
	array('label'=>'Crear Categoria Documento','url'=>array('create')),
	array('label'=>'Modificar Categoria Documento','url'=>array('update','id'=>$model->idCategoriaDocumento)),
	array('label'=>'Eliminar Categoria Documento','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idCategoriaDocumento),'confirm'=>'Está seguro de eliminar este item?')),
	//array('label'=>'Administrar CategoriaDocumento','url'=>array('admin')),
);
?>

<h1>Categoría Documento: <?php echo $model->nombreCatDoc; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		//'idCategoriaDocumento',
		'nombreCatDoc',
	),
)); ?>
