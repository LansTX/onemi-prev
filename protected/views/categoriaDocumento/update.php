<?php
$this->breadcrumbs=array(
	'Categoria Documentos'=>array('index'),
	$model->idCategoriaDocumento=>array('view','id'=>$model->idCategoriaDocumento),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Categoria Documento','url'=>array('index')),
	array('label'=>'Crear Categoria Documento','url'=>array('create')),
	array('label'=>'Ver Categoria Documento','url'=>array('view','id'=>$model->idCategoriaDocumento)),
	//array('label'=>'Administrar CategoriaDocumento','url'=>array('admin')),
);
?>

<h1>Editar Categoría de Documento: <?php echo $model->nombreCatDoc; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>