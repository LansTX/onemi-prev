<?php
$this->breadcrumbs=array(
	'Categoria Documentos',
);

$this->menu=array(
	array('label'=>'Crear CategoriaDocumento','url'=>array('create')),
	array('label'=>'Administrar CategoriaDocumento','url'=>array('admin')),
);
?>

<h1>Categoria Documentos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
