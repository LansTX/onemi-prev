<?php
$this->breadcrumbs=array(
	'Categoria Documentos'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar CategoriaDocumento','url'=>array('index')),
	array('label'=>'Administrar CategoriaDocumento','url'=>array('admin')),
);
?>

<h1>Nueva Categoría de Documento</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>