<?php

class DocumentoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','download'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','download','documentossubidos'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','download'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Documento;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Documento']))
		{
			$model->attributes=$_POST['Documento'];
			$rnd = rand(0,99999);

			$uploadedFile = CUploadedFile::getInstance($model,'rutaDoc');
			$fileName = "{$rnd}-{$uploadedFile}";

            $model->rutaDoc = $fileName;
            $bytes = $uploadedFile->getSize();
            $bytesString = Util::sizeFile($bytes);
            $model->sizeDoc = $bytesString;

			if($model->save()){
				$uploadedFile->saveAs(Yii::app()->basePath.'/../archivos/documentos/'.$fileName);
				$this->redirect(array('view','id'=>$model->idDocumento));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Documento']))
		{
			$_POST['Banner']['rutaDoc'] = $model->rutaDoc;
			$model->attributes=$_POST['Documento'];

			$uploadedFile=CUploadedFile::getInstance($model,'rutaDoc');

			if($model->save()){
				if(!empty($uploadedFile))  // check if uploaded file is set or not
                {
                    $uploadedFile->saveAs(Yii::app()->basePath.'/../archivos/documentos/'.$model->rutaDoc);
                }
				$this->redirect(array('view','id'=>$model->idDocumento));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Documento');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionDocumentosSubidos()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = "Usuario_idUsuario=:idUsuario";
		$criteria->params = array(':idUsuario' => Yii::app()->user->getId());

		$dataProvider=new CActiveDataProvider('Documento',array(
			'criteria'=>$criteria,
			));

		$this->render('documentospropios',array(
			'dataProvider'=>$dataProvider,
			'criteria'=>$criteria,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Documento('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Documento']))
			$model->attributes=$_GET['Documento'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Documento::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='documento-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionDownload($id){
		$model=$this->loadModel($id);
		$path = Yii::getPathOfAlias('webroot')."/archivos/documentos/".$model->rutaDoc;
		//$path = Yii::app()->basePath.'/../archivos/documentos/'.$model->rutaDoc;
		
  		$this->downloadFile($path);
	}

	public function downloadFile($fullpath){
  if(!empty($fullpath)){ 
      header("Content-type:application/pdf"); //for pdf file
      //header('Content-Type:text/plain; charset=ISO-8859-15');
      //if you want to read text file using text/plain header 
      header('Content-Disposition: attachment; filename="'.basename($fullpath).'"'); 
      header('Content-Length: ' . filesize($fullpath));
      readfile($fullpath);
      Yii::app()->end();
  }
}
}
