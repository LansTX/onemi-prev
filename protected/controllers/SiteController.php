	<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			/*'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			), 
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName*/
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];

			if($model->validate()){
				Yii::import('application.extensions.phpmailer.JPhpMailer');
				$mail = new JPhpMailer;
				$mail->IsSMTP();
				$mail->Host = 'smtp.googlemail.com:465';
				$mail->SMTPAuth = true;
				$mail->SMTPSecure = "ssl";
				$mail->Username = 'dummydummy331@gmail.com';
				$mail->Password = 'imaqtpie';
				$mail->SetFrom($model->email, '=?UTF-8?B?'.base64_encode($model->nombre).'?=');
				$mail->Subject = '=?UTF-8?B?'.base64_encode($model->asunto).'?=';
				$mail->MsgHTML($model->mensaje);
				$mail->AddAddress(Yii::app()->params['adminEmail'], 'SIJUDA');
				$mail->Send();
				Yii::app()->user->setFlash('contact','Gracias por enviarnos su mensaje. Le responderemos en cuanto nos sea posible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}


	
	/**
     * Declares class-based actions.
     */

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm();

		if(Yii::app()->user->isGuest){
			// if it is ajax validation request
			if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}

			// collect user input data
			if(isset($_POST['LoginForm'])){
				$model->attributes=$_POST['LoginForm'];
				// se toma el valor ingresado en el formulario y se encripta con md5
				$passSinMd5 = $model->pass; // Se guarda la pass para retornarlaencaso de error
				$model->pass = md5($model->pass); 		
				$validate = $model->validate();
				$login = $model->login();
				if($login) {
					$this->redirect(array('site/index'));
				}
				else $model->pass = $passSinMd5;			
			}
			// display the login form
			$this->render('login',array('model'=>$model));
		}
		else $this->redirect(array('site/index'));
	}
	// Cierra la sesión 
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}	

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}