<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/estilos.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tabla.css" />
	<!-- 
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/material/assets/css/material-kit.css" rel="stylesheet"/>
	-->
	
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-awesome.css" />
	
   <!--<script src="https://use.fontawesome.com/7d96620d00.js"></script>-->

   <title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>

    <script type="text/javascript">
    	function myFunction() {
		    document.getElementById("myDropdown").classList.toggle("show");
		}

		window.onclick = function(event) {
		  if (!event.target.matches('.dropbtn')) {

		    var dropdowns = document.getElementsByClassName("dropdown-content");
		    var i;
		    for (i = 0; i < dropdowns.length; i++) {
		      var openDropdown = dropdowns[i];
		      if (openDropdown.classList.contains('show')) {
		        openDropdown.classList.remove('show');
		      }
		    }
		  }
		}
    </script>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />
</head>

<body id="fondo">


<div id="pagetop">

	<div id="logo">
		<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/onemi_logo.jpg" href="#"/>
	</div>
	<!--
	<div class="titletop">
		ONEMI Arica: Prevención
	</div>
	-->
	<div id="logininfo">

	<?php if (!Yii::app()->user->isGuest):?>

		<div class="imguser ib" onclick="myFunction()">
			<i class="fa fa-sort-desc fa-2x" aria-hidden="true"></i>
			<div id="myDropdown" class="dropdown-content">
    		<a href="<?php echo Yii::app()->createUrl("usuario/update",array('id'=>Yii::app()->user->getId()));?>">Editar Perfil</a>
    		<a href="<?php echo Yii::app()->createUrl("site/logout",array());?>">Cerrar Sesión</a>
  </div>
		</div>


		
		<div class="ib">
			<?php echo Yii::app()->user->getNombre(Yii::app()->user->getId());?>
		</div><br/>

		<div class="optionsuser ib" style="display:none;">
			<a href="<?php echo Yii::app()->createUrl("usuario/update",array('id'=>Yii::app()->user->getId()));?>" >Editar</a> |
			<a href="<?php echo Yii::app()->createUrl("site/logout",array());?>" >Desconexión</a>
		</div>
	

	<?php elseif (Yii::app()->user->isGuest):?>
	
		<div class="userlogin ib">
			
			<i class="fa fa-user" aria-hidden="true"></i>
			<a href="<?php echo Yii::app()->createUrl("site/login",array());?>" >Iniciar Sesión</a>

		</div>

	<?php endif;?>

	</div>
</div>
	<div class="contenidobox">

		<div id="menuizq">

			
			<ul>
				<li><a class="btnTipo1" href="<?php echo Yii::app()->createUrl("site/index",array());?>">Inicio</a></li>
				<li><a class="btnTipo2" href="<?php echo Yii::app()->createUrl("usuario/index",array());?>">Usuarios</a></li>
				<li><a class="btnTipo3" href="<?php echo Yii::app()->createUrl("alerta/index",array());?>">Alertas</a></li>
				<li><a class="btnTipo4" href="<?php echo Yii::app()->createUrl("noticia/index",array());?>">Noticias</a></li>
				<li><a class="btnTipo5" href="<?php echo Yii::app()->createUrl("documento/index",array());?>">Archivos</a></li>
				<li><a class="btnTipo6" href="<?php echo Yii::app()->createUrl("capacitacion/index",array());?>">Capacitaciones</a></li>
				<li><a class="btnTipo7" href="<?php echo Yii::app()->createUrl("zonasegura/index",array());?>">Mapas</a></li>
				<li><a class="btnTipo7" href="<?php echo Yii::app()->createUrl("ruta/index",array());?>">Rutas Seguras</a></li>
				<li><a class="btnTipo7" href="<?php echo Yii::app()->createUrl("zonasegura/test",array());?>">Zonas Peligrosas</a></li>
				<li><a class="btnTipo7" href="<?php echo Yii::app()->createUrl("zonasegura/test2",array());?>">Zonas Peligrosas</a></li>
				<li><a class="btnTipo5" href="<?php echo Yii::app()->createUrl("categoria/index",array());?>">Categorías</a></li>
			</ul>

	    	<div class="operaciones">
	    		<ul>
	        		<li><a href="#">OPERACIONES</a></li>
	        	</ul>

	    	</div>

	    	<div class="menuvariable" style="float:left;">

	        	<div id="sidebar">
	        
		        	<?php
		            
		            	$this->beginWidget('zii.widgets.CPortlet', array(
		                	'htmlOptions' => array('class'=>'div-operaciones'),
		            	));
		            
		            	$this->widget('bootstrap.widgets.TbMenu', array(
		                    'items'=>$this->menu,
		                    'htmlOptions'=>array('class'=>'operations'),
		                ));
		            
		            
		            	$this->endWidget();
		        	?>
	        	</div><!-- sidebar -->
	    	</div>
	    
		</div>
	
		<div id="pagecontent">

			<div class="row">

	    		<div class="span10">

	        		<div id="content">

	            		<?php echo $content; ?>

	        		</div><!-- content -->
	    		</div>
	    
			</div>
		</div>
	
		

	</div>


<div class="footerprev" align="center" style="display:none;">
	
	<h4 class="title">Información</h4>

		<p>
		Teléfono: (56) 582248598 
		<br>
		Oficina regional: Av. 18 de septiembre 2071 - Arica
		<br>
		Email: contacto@onemiprev.cl
		<br>
		onemiprev@onemiprev.cl
		</p>

	</div>

</div><!-- footer -->


<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scripts.js"></script>

</body>

</html>