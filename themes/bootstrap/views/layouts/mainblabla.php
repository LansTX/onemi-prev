<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/estilos.css" />
	<!-- 
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/material/assets/css/material-kit.css" rel="stylesheet"/>
	-->
	<!--
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-awesome.min.css" />
	-->
	
<script src="https://use.fontawesome.com/7d96620d00.js"></script>

   <title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>

    
	
</head>

<body>
<div>
<div id="pagetop">
	<img src="<?php echo Yii::app()->theme->baseUrl; ?>/css/logo2.png" href="#"/>
	<div class="titletop iblock">
		ONEMI Fase de Prevención
	</div>
	<?php if (!Yii::app()->user->isGuest):?>
	<div id="logininfo">
		<div class="imguser iblock">
		<i class="fa fa-user fa-2x" aria-hidden="true"></i>
		</div>
		
		<div class="blockstep iblock">
		<?php echo Yii::app()->user->getNombre(Yii::app()->user->getId());?>
		</div><br/>
		<div class="optionsuser iblock">
		<a href="<?php echo Yii::app()->createUrl("usuario/update",array('id'=>Yii::app()->user->getId()));?>" >Editar</a> |
		<a href="<?php echo Yii::app()->createUrl("site/logout",array());?>" >Desconexión</a>
		</div>
	</div>
	<?php elseif (Yii::app()->user->isGuest):?>
	
		<div class="userlogin iblock">
		<i class="fa fa-user" aria-hidden="true"></i>
			<a href="<?php echo Yii::app()->createUrl("site/login",array());?>" >Iniciar Sesión</a>
		</div>
	<?php endif;?>
</div>
<div class="clear" style="clear:both;"></div>


<div class="contenidobox">

	<div id="menuizq">
		
			<ul>
				<li><a href="<?php echo Yii::app()->createUrl("site/index",array());? is>">Inicio</a></li>
				<li><a href="<?php echo Yii::app()->createUrl("usuario/index",array());?>">Usuarios</a></li>
				<li><a href="#">Alertas</a></li>
				<li><a href="#">Noticias</a></li>
				<li><a href="#">Archivos</a></li>
				<li><a href="#">Capacitaciones</a></li>
				<li><a href="<?php echo Yii::app()->createUrl("zonasegura/create",array());?>">Mapas</a></li>
			</ul>
	</div>
	
	<div id="pagecontent">
		<?php echo $content; ?>
	</div>
	

	<div class="clear"></div>

	<br/>
	<br/><br/>
	

</div>
</div>
<div class="footerprev" align="center">
	
<h4 class="title">Información</h4>

<p>
Teléfono: (56) 582248598 
<br>
Oficina regional: Av. 18 de septiembre 2071 - Arica
<br>
Email: contacto@onemiprev.cl
<br>
onemiprev@onemiprev.cl
</p>

</div>
</div><!-- footer -->


<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scripts.js"></script>
</body>
</html>
