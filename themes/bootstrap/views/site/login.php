<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Iniciar Sesión';
$this->breadcrumbs=array(
	'Iniciar Sesión',
);
?>

<h1>Iniciar Sesión</h1>

<p>Complete el siguiente formulario con sus datos de acceso:</p>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'login-form',
    'type'=>'horizontal',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->textFieldRow($model,'usuario'); ?>

	<?php echo $form->passwordFieldRow($model,'pass'); ?>

	<?php /*echo $form->checkBoxRow($model,'rememberMe');*/ ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Ingresar',
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
