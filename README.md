El layout principal se encuentra en themes\bootstrap\views\layouts\main.php.

Los archivos css, imgs, fonts, etc. nuevos, se encuentran en themes/bootstrap/.

Todas las vistas se encuentran en protected/views/, con excepción de la vista de login y la página de inicio que se encuentran en themes/bootstrap/views/site/ login.php e index.php. Las vistas creadas bajo la carpeta themes/bootstrap/views/ tienen prioridad sobre las creadas en protected/views.

Teniendo esta URL onemiprev/index.php/**zonasegura**/**create**, para encontrar la vista asociada primero se debe:

* Ir protected/controllers/ y abrir el archivo **ZonaSegura**Controller.php.

* Buscar una función con nombre action**Create**.

* Por lo general, al final de la declaración de la función existe una línea similar a esta:  

     $this->render('***create***',array('model'=>$model,));

* La vista asociada se encontrará en protected/views/zonasegura/***create***.php. Dentro de esta vista puede renderizar a otra vista del mismo controlador u otro, por lo general tiene este formato:

      $this->renderPartial('**_form**', array('model'=>$model));

* Para este caso, me indica que se debe renderizar la vista **_form**, ubicada en protected/views/zonasegura.